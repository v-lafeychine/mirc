\documentclass[a4paper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}

\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{minted}

\hypersetup{
    colorlinks=true,
    linkcolor=red,
    urlcolor=blue
}

\title{Rapport de projet réseaux : MIRC}
\author{Vincent Lafeychine \and Balthazar Patiachvili}
\date{}

\begin{document}

    \maketitle

    \begin{center}
        Lien vers le dépôt git : \url{https://gitlab.crans.org/v-lafeychine/mirc}
    \end{center}

    \tableofcontents

    \section{Présentation du projet}

        \subsection{Généralités}

            Voici le projet réseaux MIRC de Vincent Lafeychine et Balthazar Patiachvili. Il a été réalisé en Rust, et toutes les spécifications demandées ont été implémentées. De plus, il dispose d'une interface utilisateur dans le terminal de commande (TUI) pour faciliter son utilisation et la compréhension de toutes les actions effectuées par le programme.

        \subsection{Langage}

            Le choix du langage s'est porté sur Rust pour plusieurs raisons.

            D'une part, il s'agit d'un langage compilé dont l'exécution est rapide tel que peut l'offrir le langage C, usuellement utilisé pour de tels projets.

            D'autre part, Rust offre la possibilité d'utiliser le paradigme concurrent, qui couplé à ses propriétés concernant la mémoire, nous permet d'interagir sereinement avec les multiples \emph{green threads} tout en ayant une gestion fine des erreurs.

        \subsection{Compilation}

            La compilation passe par cargo, le package manager de Rust\footnote{Voir ici : \url{https://doc.rust-lang.org/cargo/}}. La compilation et l'exécution sont détaillées dans le fichier \texttt{README.md} à la racine du dossier.

        \subsection{Utilisation}

            \subsubsection{Interface utilisateur}
                
                L'interface utilisateur a été inspirée de l'interface du client IRC Weechat, et se compose en trois parties : l'entrée utilisateur en bas de l'écran, la liste des différents chats à gauche de l'écran, et le chat courant qui occupe la majeure partie de l'écran.

                Le chat courant est affiché en gras dans la liste des chats, et il est possible de parcourir la liste des chats en appuyant sur \texttt{Alt+Up/Down Arrow}.

                La liste des chats présents est la suivante : le chat <<~System~>>, le chat <<~Everyone~>>, ainsi qu'un chat par voisin ayant envoyé un TLV Hello. La description de tous ces chats est détaillée ci-dessous.

                Il est possible de quitter cette interface ainsi que le programme à tout moment en appuyant sur \texttt{Ctrl+C}.

            \subsubsection{Messages}

                Dans un chat, tous les messages se présentent sous la même forme : le nom d'utilisateur est affiché en vert, l'heure de récéption du message en cyan, et le message correspondant en blanc sur la ligne d'après.

                Certains messages peuvent apparaître en rouge : il s'agit des messages envoyés par l'utilisateur qui n'ont été reçu par aucun autre hôte (plus précisément ceux pour lesquels aucun TLV Ack n'a été reçu).

            \subsubsection{Entrées utilisateur}

                Pour envoyer un message, il suffit de se placer dans le chat correspondant, puis d'écrire directement le message, qui apparaîtra alors dans la partie du TUI correspondant à l'entrée utilisateur. Pour envoyer le message, il faut ensuite appuyer sur \texttt{Entrée}.

                Le nom d'utilisateur est actuellement défini aléatoirement, et est affiché en haut du cadre de l'entrée utilisateur.

            \subsubsection{Chat <<~System~>>}

                Ce chat reçoit tous les messages envoyés par le système. Il affiche notamment tous les TLV envoyés et reçus, ainsi que l'état des messages inondés et certains TLV envoyés par des hôtes inconnus. 
                
                À noter qu'il n'est pas possible d'envoyer des messages dans ce chat.

            \subsubsection{Chat <<~Everyone~>>}

                Ce chat est le chat commun à tous les hôtes, et correspond aux spécifications de MIRC, à savoir : tous les messages envoyés et reçus ici sont transmis à tous les voisins avec le principe d'inondation. Celui-ci est d'avantage décrit ci-dessous.

            \subsubsection{Chats utilisateurs}

                Ces chats constituent une extension du protocole : les messages privés envoyés à un unique utilisateur. Pour envoyer un message privé à un utilisateur, il suffit de se placer dans le chat correspondant, puis d'envoyer un message qui ne sera transmis qu'à cet utilisateur. Les détails de cette extension sont décrit ci-dessous.

    \section{Choix d'implémentations}

        \subsection{Programmation asynchrone}

                Les applications en réseaux doivent manipuler simulanément plusieurs \emph{sockets}. Il existe plusieurs architectures de programmes pour manipuler les \emph{sockets}, car une application peut se retrouver bloquée dans l'attente d'un flux (par exemple, en attente d'une lecture dans l'appel système \texttt{recvfrom}), empêchant toute autre communication avec les autres \emph{sockets}. \\

                Une première architecture consiste à créer un \emph{thread} pour chaque nouvelle connexion. Si un \emph{thread} se trouve dans l'attente d'un flux, les autres \emph{threads} peuvent continuer de s'exécuter et de communiquer.
                Cependant, cette architecture requiert beaucoup de mémoire (duplication de la mémoire virtuelle) et de calculs (changement de contexte fréquent dans l'enceinte de l'application). \\

                Une deuxième architecture consiste à demander au système d'exploitation quel \emph{socket} est disponible à la lecture, en utilisant un moniteur tel que \texttt{select}.
                Cependant, cela requiert qu'il y ait un unique point d'entrée et de sortie de communication dans l'application, ce qui rend très complexe les communications imbriquées (tel un protocole). \\

                Une dernière possibilité est d'utiliser un langage concurrent, où les deux premières architectures se rejoignent.
                Un langage concurrent intègre un \emph{runtime} intégrant un moniteur où toutes les opérations bloquantes (tel que \texttt{recvfrom}) y sont envoyés. Le \emph{runtime}, en attendant une réponse l'une des opérations bloquantes, intègre également un \emph{scheduler} qui donne la main à une autre série d'opérations à effectuer (\emph{green threads}). \\

                Nous avons utilisé \texttt{tokio} (\url{https://tokio.rs/}) qui est un \emph{runtime} populaire dans l'écosystème Rust. De plus, Rust contraint les objets qui peuvent être communiqués (via les traits \texttt{Send} et \texttt{Sync}), permettant de s'assurer de la cohérence des actions effectuées.

        \subsection{Sérialisation et désérialisation}

            Dans ce projet, la sérialisation et la désérialisation passent par le package \texttt{serde} de Rust\footnote{Voir ici : \url{https://serde.rs/}}, qui offre plusieurs avantages.

            Tout d'abord, il est possible d'ajouter très simplement de nouveaux TLV, et ainsi d'étendre le protocole. Les sérialisation et désérialisation des messages sont génériques et ne dépendent que de celles des TLV.

            De plus, le caractère générique permet également de changer le format de sortie sans difficulté : il est facilement possible de convertir les messages sous un format différent comme JSON, YAML, \dots
            
            Par ailleurs, de nombreux tests unitaires ont été créés dans cette partie pour vérifier que l'intégralité des TLV puissent se sérialiser vers et se désérialiser à partir d'une suite d'octets.
            
        \subsection{Interface utilisateur}

            Pour l'interface utilisateur, le package \texttt{tui}\footnote{Voir la documentation ici : \url{https://docs.rs/tui/latest/tui/index.html}} a été utilisé. Celui-ci permet de réaliser sans difficulté une interface utilisateur dans un terminal, ce qui est plus agréable d'utilisation qu'une interface en ligne de commandes.

        \subsection{Découverte de voisins}

            La découverte de voisins peut se faire de deux manières : la déclaration initiale des voisins potentiels lors du lancement du programme, ou la récéption d'un TLV Neighbour. Afin de permettre la découverte rapide de nouveaux voisins, des TLV Neighbour sont envoyées toutes les 10 secondes.

        \subsection{Inondation}

            Toutes les secondes, la liste des messages à inonder est parcourue et est mise à jour de la façon suivante :

            \begin{itemize}
                \item si le temps écoulé depuis la dernière inondation est supérieure à une durée aléatoire comprise entre $2^n$ et $2^{n+1}$, alors le message est renvoyé et une nouvelle durée est générée en incrémentant $n$ de $1$~;
                \item si $n$ atteint $5$, alors on attend une durée aléatoire générée de la même façon, et si aucun TLV n'est venu retirer ce message de la liste à inonder, un TLV GoAway de code 2 est envoyé~;
                \item les messages sont envoyés en utilisant des \texttt{mpsc}, à savoir des \textit{multi-producer, single-consumer FIFO queue communication primitives}, permettant de communiquer des instructions de manière asynchrone à différentes parties du programme. Cela permet d'envoyer et de recevoir des messages, en parallèle sans que l'attente ne soit bloquante.
            \end{itemize}

            Par ailleurs, tous les messages reçus sont retransmis s'ils n'ont pas déjà été reçus.

    \section{Extension}

        \subsection{Mesages privés}

            Un nouveau TLV a été ajouté : le TLV Whisper, qui se présente de la façon suivante :

            \begin{itemize}
                \item le tag sur 1 octet, valant toujours 8~;
                \item la longueur du corps sur 1 octet~;
                \item l'ID de l'envoyeur sur 8 octets~;
                \item l'ID du destinataire sur 8 octets~;
                \item le contenu du message sous format UTF-8.
            \end{itemize}

            Ces messages doivent également étre inondés, mais il ne faut pas les transmettre à tous ses voisins : si un hôte différent du destinataire reçoit le message, il envoie un TLV Warning et ignore le TLV.

        \subsection{Sécurité de l'implémentation}

            De part les choix d'implémentation, il n'est pas possible de traiter un message ayant une taille supérieure à $2^{16}$ octets, et un TLV ayant une taille supérieure à $2^8$ octets. Cela n'entraîne pas d'erreur, mais va simplement ne pas prendre en compte ces paquets envoyés. Pour les paquets reçus, si la taille dépasse la taille maximale, le message ne sera pas traité et un TLV GoAway de code 3 sera envoyé.

        \subsection{Concurrence}

            Cette implémentation d'IRC est capable de gérer plusieurs inondations en parallèle sans difficulté : une liste des messages à inonder est parcourue toutes les secondes, et le message correspondant est envoyé au destinataire sans que cela ne bloque le pair.

\end{document}
