# MIRC

A MIRC client developped for the Network course, during the first year of the MPRI in the ENS Paris-Saclay.

It has been created by Vincent Lafeychine and Balthazar Patiachvili.

## Build and install

To install and build the project, you may use the following commands :

```bash
git clone https://gitlab.crans.org/v-lafeychine/mirc.git

cd mirc

cargo build
```

## Run

```bash
cargo run -- [<IPv6 address>]:<port> <optional potential neighbours>
```

For example, to create three instances communicating locally, it is possible to use :

```bash
cargo run -- [::1]:10000
```

```bash
cargo run -- [::1]:8000 [::1]:10000
```

```bash
cargo run -- [::1]:12000 [::1]:10000
```

## Unitary tests

Unitary tests can be tested with :

```bash
cargo test
```