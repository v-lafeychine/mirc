pub mod neighbour;
pub mod node;

use std::net::{SocketAddr, SocketAddrV6};

use anyhow::Result;
use log::{debug, info, trace, warn};
use serde::{Deserialize, Serialize};
use tokio::net::UdpSocket;
use tokio::sync::mpsc::{Receiver, UnboundedReceiver, UnboundedSender};
use tokio::time::{interval, sleep, Duration};

use self::neighbour::Neighbour;
pub use self::node::Data;
use self::node::Node;
use crate::payload::message::Message;
use crate::payload::serializer::ByteString;
use crate::payload::Tlv;
use crate::server::node::DataType;

/// Commands that can be sent after different actions and that will be executed on a separated thread
///
/// Usefull to transmit the need to send and/or handle a TLV
#[derive(Debug, Eq, PartialEq)]
pub enum Command {
    /// Sends a ping to all neighbours and potential neighbours
    PingNeighbours,

    /// Sends neighbour list to all neighbours
    SendNeighbours,

    /// Begin flooding
    Flooding,

    /// Flood everyone with a message created
    CreateFlood(u32, String),

    /// Flood everyone with a message received
    TransmitFlood(u64, u32, String),

    /// A TLV has been received from a host and has to be handle
    TlvReceived(SocketAddrV6, Tlv),

    /// Acknowledge a message `(id, nonce)` sent by a neighbour
    SendAck(Neighbour, u64, u32),

    /// Sends a GoAway TLV to a neighbour
    SendGoAway(SocketAddrV6, u8, String),

    /// Sends a Warning TLV to a neighbour
    SendWarning(SocketAddrV6, String),

    /// Send a GoAway TLV with code 1
    Leave,

    /// Whispers to a neighbour
    Whisper(Neighbour, u32, String),
}

/// Handle received commands
pub async fn dispatcher(
    mut node: Node,
    mut receiver: UnboundedReceiver<Command>,
    sender_command: UnboundedSender<Command>,
    sender_data: UnboundedSender<Data>,
    system: Neighbour,
) -> Result<()> {
    loop {
        let (duration, host) = node.next_neighbour_timeout();

        debug!("Next host timeout is {:?} in {:?}", host, duration);

        tokio::select! {
            _ = sleep(duration) => {
                let Some(host) = host else {
                    unreachable!("Infinity sleep ended");
                };

                node.host_timeout(host).await?;
            },

            command = receiver.recv() => {
                let Some(command) = command else {
                    info!("Receiver channel closed");
                    break;
                };

                debug!("Received command: {:?}", command);

                match command {
                    Command::PingNeighbours => {
                        node.ping_neighbours().await?;
                    },
                    Command::SendNeighbours => {
                        node.send_neighbours().await?;
                    },
                    Command::Flooding => {
                        node.flooding(sender_command.clone(), sender_data.clone(), system.clone()).await?;
                    }
                    Command::CreateFlood(nonce, message) => {
                        let neighbours = node.get_neighbours();
                        for neighbour in neighbours {
                            node.send_data(&neighbour, message.clone(), Some((node.id, nonce)), false).await?;
                        }
                    }
                    Command::TransmitFlood(id, nonce, message) => {
                        let neighbours = node.get_neighbours();
                        for neighbour in neighbours {
                            node.send_data(&neighbour, message.clone(), Some((id, nonce)), true).await?;
                        }
                    }
                    Command::TlvReceived(host, tlv) => {
                        node.handle(host, tlv, sender_command.clone(), sender_data.clone(), system.clone()).await?;
                    },
                    Command::Whisper(neighbour, nonce, data) => {
                        node.send_whisper(&neighbour, nonce, data).await?;
                    }
                    Command::SendAck(neighbour, id, nonce) => {
                        node.send_ack(neighbour, id, nonce).await?;
                    }
                    Command::SendGoAway(host, code, message) => {
                        node.send_go_away(host, code, message).await?;
                    }
                    Command::SendWarning(host, message) => {
                        node.send_warning(host, message).await?;
                    }
                    Command::Leave => {
                        let neighbours = node.get_neighbours();
                        for neighbour in neighbours {
                            node.send_go_away(neighbour.host, 1, "I left the chatroom".to_owned()).await?;
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

/// This function send `TLV Hello` to all neighbours, every each 30 seconds.
pub async fn heartbeat_alive(sender: UnboundedSender<Command>) -> Result<()> {
    let mut interval = interval(Duration::from_secs(30));

    loop {
        interval.tick().await;

        sender.send(Command::PingNeighbours)?;
    }
}

/// This function send `TLV Neighbour` to all neighbours, every each `n` seconds.
pub async fn heartbeat_sending(n: Duration, sender: UnboundedSender<Command>) -> Result<()> {
    let mut interval = interval(n);

    loop {
        interval.tick().await;

        sender.send(Command::SendNeighbours)?;
    }
}

/// Check every second if there is a message to flood
pub async fn flooding(sender: UnboundedSender<Command>) -> Result<()> {
    let mut interval = interval(Duration::from_secs(1));

    loop {
        interval.tick().await;

        sender.send(Command::Flooding).unwrap();
    }
}

/// Only function that communicates with the world
pub async fn world(
    socket: UdpSocket,
    mut receiver: Receiver<(SocketAddrV6, Tlv)>,
    sender_command: UnboundedSender<Command>,
    sender_data: UnboundedSender<Data>,
    system: Neighbour,
) -> Result<()> {
    let mut buf = [0; 1024];

    loop {
        tokio::select! {
            incoming_data = socket.recv_from(&mut buf) => {
                let (n, src) = match incoming_data {
                    Ok((n, src)) => (n, src),
                    Err(_) => continue,
                };

                trace!("Received data {:?}", buf[..n].to_vec());

                let SocketAddr::V6(src) = src else {
                    warn!("Received data from non-IPv6 source: {:?}", src);
                    continue;
                };


                let message = match Message::deserialize(&mut ByteString::new(buf[..n].to_vec())) {
                    Ok(message) => message,
                    Err(_) => {
                        sender_command.send(Command::SendGoAway(src, 3, "The protocol has been violated : bad message format".to_owned()))?;
                        continue
                    }
                };

                if message.magic != 95 {
                    warn!("Received data with incorrect magic number: {:?}", src);
                    continue;
                }

                if message.version != 0 {
                    warn!("Received data with incorrect version protocol: {:?}", src);
                    continue;
                }

                for tlv in message.body {
                    trace!("Received data from {:?}: {:?}", src, tlv);

                    sender_data
                        .send(Data {
                            neighbour: system.clone(),
                            content: DataType::System(format!("TLV received from {src:?}: {tlv:?}")),
                        })?;

                        sender_command.send(Command::TlvReceived(src, tlv))?;
                }
            }

            msg = receiver.recv() => {
                let Some((host, tlv)) = msg else {
                    info!("Receiver channel closed");
                    break;
                };

                trace!("Sending: {:?}", tlv);

                sender_data.send(Data {
                    neighbour: system.clone(),
                    content: DataType::System(format!("TLV sent to {host:?} : {tlv:?}")),
                })?;

                let mut message = Message::default();
                message.add_tlv(tlv);

                let mut buffer = ByteString::default();
                match &message.serialize(&mut buffer) {
                    Ok(_) => (),
                    Err(_) => {
                        sender_data.send(Data {
                            neighbour: system.clone(),
                            content: DataType::System("Tried to send a message too long".to_owned())
                        })?;
                        continue
                    }
                };

                socket.send_to(&buffer.get(), host).await?;
            },
        };
    }

    Ok(())
}
