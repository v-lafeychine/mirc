use std::net::Ipv6Addr;

use derive_more::Constructor;

#[derive(Clone, Copy, Constructor, Debug)]
pub struct Host {
    address: Ipv6Addr,
    port: u16,
}

impl Host {
    pub fn socket_addr(&self) -> (Ipv6Addr, u16) {
        (self.address, self.port)
    }
}
