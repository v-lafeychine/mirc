use std::net::SocketAddrV6;

use chrono::{DateTime, Utc};

/// Structure for neighbours
#[derive(Debug, Clone)]
pub struct Neighbour {
    pub id: u64,

    /// Last known socket
    pub host: SocketAddrV6,

    /// Last known nickname
    pub nick: String,

    /// Last received Hello
    last_seen: DateTime<Utc>,

    /// Last received Hello with a correct Id
    last_symmetric: Option<DateTime<Utc>>,
}

impl PartialEq for Neighbour {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.host == other.host
    }
}

impl Eq for Neighbour {}

impl Neighbour {
    pub fn new(id: u64, host: SocketAddrV6, nick: String) -> Self {
        Self {
            id,
            host,
            nick,
            last_seen: Utc::now(),
            last_symmetric: None,
        }
    }

    pub fn is_symmetric(&self) -> bool {
        let Some(last_symmetric) = self.last_symmetric else { return false; };

        Utc::now().signed_duration_since(last_symmetric).num_minutes() < 2
    }

    pub fn last_seen(&self) -> DateTime<Utc> {
        self.last_seen
    }

    pub fn update(&mut self, is_symmetric: bool) {
        let now = Utc::now();

        self.last_seen = now;

        if is_symmetric {
            self.last_symmetric = Some(now);
        }
    }
}
