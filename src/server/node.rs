use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::net::{IpAddr, SocketAddrV6};
use std::time::Duration;

use anyhow::Result;
use chrono::Utc;
use circular_queue::CircularQueue;
use log::{info, warn};
use rand::Rng;
use tokio::sync::mpsc::{Sender, UnboundedSender};

use super::Command;
use crate::payload::tlv::Instantiable;
use crate::payload::{tlv, Tlv};
use crate::server::neighbour::Neighbour;

/// Structure for our node
#[derive(Debug)]
pub struct Node {
    pub id: u64,
    sender: Sender<(SocketAddrV6, Tlv)>,
    host: SocketAddrV6,
    nick: String,

    potential: HashSet<SocketAddrV6>,
    neighbours: HashMap<SocketAddrV6, Neighbour>,

    /// Neighbours to flood
    ///
    /// For each neighbour, contains the duration until its next flooding, the timestamp at which the timer began, how many
    /// times the message has been sent and the message to send
    #[allow(clippy::type_complexity)]
    to_flood: HashMap<(SocketAddrV6, u64, u32), (Duration, i64, u32, Tlv)>,

    last_received: CircularQueue<(u64, u32)>,
}

/// Datas transmitted to the TUI
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Data {
    /// Indicates the tab on which the content will be displayed
    pub neighbour: Neighbour,

    /// Content of the data
    pub content: DataType,
}

/// Types and contents of datas transmitted to the TUI
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum DataType {
    /// Message received on "Everyone" channel
    Message(u64, u32, String),

    /// Whisper reveiced
    Whisper(u64, u32, String),

    /// System message
    System(String),

    /// Creates a new tab
    NewNeighbour,

    /// Indicates that the message `(id, nonce)` has been received
    ReceivedMessage(u64, u32),
}

impl Node {
    pub fn new(sender: Sender<(SocketAddrV6, Tlv)>, host: SocketAddrV6, nick: String, potential: HashSet<SocketAddrV6>) -> Self {
        Self {
            id: rand::thread_rng().gen(),
            host,
            sender,
            nick,
            potential,
            neighbours: HashMap::new(),
            to_flood: HashMap::new(),
            last_received: CircularQueue::with_capacity(32),
        }
    }

    /// Returns a copy of all neighbours
    pub fn get_neighbours(&self) -> Vec<Neighbour> {
        self.neighbours.values().cloned().collect()
    }

    /// Timeouts a neighbour
    pub async fn host_timeout(&mut self, host: SocketAddrV6) -> Result<()> {
        let Some(neighbour) = self.neighbours.remove(&host) else {

            unreachable!("Host timeout for unknown host");
        };

        info!("Neighbour timeout {:?}", neighbour);

        self.sender.send((host, Tlv::GoAway(tlv::GoAway::new((2, "Timeout".to_string()))))).await.map_err(|e| e.into())
    }

    /// Returns a bool whether the message `(id, nonce)` has already been received or not
    fn has_already_been_received(&self, id: u64, nonce: u32) -> bool {
        for el in self.last_received.iter() {
            if el == &(id, nonce) {
                return true;
            }
        }
        false
    }

    /// Handle a received TLV
    pub async fn handle(
        &mut self,
        host: SocketAddrV6,
        tlv: Tlv,
        sender_command: UnboundedSender<Command>,
        sender_data: UnboundedSender<Data>,
        system: Neighbour,
    ) -> Result<()> {
        match tlv {
            Tlv::Pad1(_) | Tlv::PadN(_) => (),

            Tlv::Hello(hello) => self.handle_hello(host, hello, sender_data).await?,

            Tlv::Neighbour(tlv::Neighbour { addr, port }) => {
                if let IpAddr::V6(addr) = addr {
                    self.potential.insert(SocketAddrV6::new(addr, port, 0, 0));
                } else {
                    warn!("Received neighbour with non-IPv6 address");
                }
            },

            Tlv::Data(tlv::Data {
                sender_id,
                nonce,
                data,
            }) => self.handle_data(host, sender_id, nonce, data, sender_command, sender_data, system)?,

            Tlv::Ack(tlv::Ack { sender_id, nonce }) => self.handle_ack(host, sender_id, nonce, sender_data, system),

            Tlv::GoAway(tlv::GoAway { code, message }) => self.handle_go_away(host, code, message, sender_data, system)?,

            Tlv::Whisper(tlv::Whisper {
                sender_id,
                destination_id,
                nonce,
                data,
            }) => self.handle_whisper(host, sender_id, destination_id, nonce, data, sender_command, sender_data, system)?,

            Tlv::Warning(tlv::Warning { message }) => self.handle_warning(host, message, sender_data, system)?,

            _ => (),
        }

        Ok(())
    }

    /// Handles a Hello TLV
    async fn handle_hello(&mut self, host: SocketAddrV6, hello: tlv::Hello, sender_data: UnboundedSender<Data>) -> Result<()> {
        let is_symmetric = Some(self.id) == hello.destination_id;

        match self.neighbours.entry(host) {
            Entry::Vacant(entry) => {
                if host != self.host {
                    let mut neighbour = Neighbour::new(hello.source_id, host, host.to_string());

                    neighbour.update(is_symmetric);

                    info!("New neighbour: {:?}", neighbour);
                    sender_data
                        .send(Data {
                            neighbour: neighbour.clone(),
                            content: DataType::NewNeighbour,
                        })
                        .expect("Broken data pipe: cannot send data");

                    if hello.destination_id.is_none() {
                        self.sender.send((host, Tlv::Hello(tlv::hello::Hello::new((self.id, Some(hello.source_id)))))).await?;
                    }

                    entry.insert(neighbour);
                }
            },

            Entry::Occupied(mut entry) => {
                entry.get_mut().update(is_symmetric);
            },
        }

        Ok(())
    }

    /// Handles a Data TLV
    #[allow(clippy::too_many_arguments)]
    fn handle_data(
        &mut self,
        host: SocketAddrV6,
        id: u64,
        nonce: u32,
        message: String,
        sender_command: UnboundedSender<Command>,
        sender_data: UnboundedSender<Data>,
        system: Neighbour,
    ) -> Result<()> {
        let Some(neighbour) = self.neighbours.get(&host) else {
            sender_command.send(Command::SendGoAway(host, 3, "The protocol has been violated : cannot receive data from an unknown host".to_owned()))?;
            return Ok(())
        };
        if neighbour.is_symmetric() {
            if !self.has_already_been_received(id, nonce) {
                let _ = self.last_received.push((id, nonce));

                sender_command.send(Command::TransmitFlood(id, nonce, message.clone()))?;
                sender_data.send(Data {
                    neighbour: neighbour.clone(),
                    content: DataType::Message(id, nonce, message),
                })?;
            } else if id == neighbour.id {
                sender_command.send(Command::SendWarning(host, "You sent a message though we are not symetric".to_owned()))?;
            }

            sender_command.send(Command::SendAck(neighbour.clone(), id, nonce))?;
        }

        sender_data.send(Data {
            neighbour: system,
            content: DataType::ReceivedMessage(id, nonce),
        })?;
        self.to_flood.remove(&(host, id, nonce));

        Ok(())
    }

    /// Sends a Data TLV
    pub async fn send_data(
        &mut self,
        neighbour: &Neighbour,
        data: String,
        predefined_arg: Option<(u64, u32)>,
        is_transmitted: bool,
    ) -> Result<()> {
        let (id, nonce) = match predefined_arg {
            Some((id, nonce)) => (id, nonce),
            None => (self.id, rand::random::<u32>()),
        };

        let tlv = Tlv::Data(crate::payload::tlv::Data::new((
            id,
            nonce,
            if is_transmitted { "".to_owned() } else { self.nick.clone() + ":" } + &data,
        )));
        self.sender.send((neighbour.host, tlv.clone())).await?;

        self.last_received.push((id, nonce));

        self.to_flood.insert((neighbour.host, id, nonce), (Duration::from_secs(1), Utc::now().timestamp(), 0, tlv));

        Ok(())
    }

    /// Handles a Ack TLV
    pub fn handle_ack(&mut self, host: SocketAddrV6, id: u64, nonce: u32, sender_data: UnboundedSender<Data>, system: Neighbour) {
        self.to_flood.remove(&(host, id, nonce));
        sender_data
            .send(Data {
                neighbour: system,
                content: DataType::ReceivedMessage(id, nonce),
            })
            .expect("Broken data type: cannot send data");
    }

    /// Sends a Ack TLV
    pub async fn send_ack(&self, neighbour: Neighbour, id: u64, nonce: u32) -> Result<()> {
        self.sender.send((neighbour.host, Tlv::Ack(tlv::Ack::new((id, nonce))))).await?;
        Ok(())
    }

    /// Handles a GoAway TLV
    pub fn handle_go_away(
        &mut self,
        host: SocketAddrV6,
        code: u8,
        message: String,
        sender_data: UnboundedSender<Data>,
        system: Neighbour,
    ) -> Result<()> {
        match self.neighbours.remove(&host) {
            None => sender_data.send(Data {
                neighbour: system,
                content: DataType::System(format!("Received GoAway TLV with code {code} from {host} : {message}")),
            }),
            Some(neighbour) => sender_data.send(Data {
                neighbour,
                content: DataType::System(format!("Received GoAway TLV with code {code} : {message}")),
            }),
        }?;

        Ok(())
    }

    /// Sends a GoAway TLV
    pub async fn send_go_away(&self, host: SocketAddrV6, code: u8, message: String) -> Result<()> {
        self.sender.send((host, Tlv::GoAway(tlv::GoAway::new((code, message))))).await?;
        Ok(())
    }

    /// Handles a Warning TLV
    pub fn handle_warning(
        &mut self,
        host: SocketAddrV6,
        message: String,
        sender_data: UnboundedSender<Data>,
        system: Neighbour,
    ) -> Result<()> {
        match self.neighbours.remove(&host) {
            None => sender_data.send(Data {
                neighbour: system,
                content: DataType::System(format!("Received Warning TLV from {host} : {message}")),
            }),
            Some(neighbour) => sender_data.send(Data {
                neighbour,
                content: DataType::System(format!("Received Warning TLV : {message}")),
            }),
        }?;

        Ok(())
    }

    /// Sends a Warning TLV
    pub async fn send_warning(&self, host: SocketAddrV6, message: String) -> Result<()> {
        self.sender.send((host, Tlv::Warning(tlv::Warning::new(message)))).await?;
        Ok(())
    }

    /// Handle a Whisper TLV
    #[allow(clippy::too_many_arguments)]
    pub fn handle_whisper(
        &mut self,
        host: SocketAddrV6,
        sender_id: u64,
        destination_id: u64,
        nonce: u32,
        message: String,
        sender_command: UnboundedSender<Command>,
        sender_data: UnboundedSender<Data>,
        system: Neighbour,
    ) -> Result<()> {
        if destination_id != self.id {
            sender_command.send(Command::SendWarning(host, "You sent a whisper to the wrong host".to_owned()))?;
            return Ok(());
        }

        let Some(neighbour) = self.neighbours.get(&host) else {
            sender_command.send(Command::SendGoAway(host, 3, "The protocol has been violated : cannot receive data from an unknown host".to_owned()))?;
            return Ok(())
        };

        if neighbour.is_symmetric() {
            if !self.has_already_been_received(sender_id, nonce) {
                let _ = self.last_received.push((sender_id, nonce));

                sender_data.send(Data {
                    neighbour: neighbour.clone(),
                    content: DataType::Whisper(sender_id, nonce, message),
                })?;
            } else if sender_id == neighbour.id {
                sender_command.send(Command::SendWarning(host, "You sent a message though we are not symetric".to_owned()))?;
            }

            sender_command.send(Command::SendAck(neighbour.clone(), sender_id, nonce))?;
        }

        sender_data.send(Data {
            neighbour: system,
            content: DataType::ReceivedMessage(sender_id, nonce),
        })?;
        self.to_flood.remove(&(host, sender_id, nonce));

        Ok(())
    }

    /// Sends a whisper TLV
    pub async fn send_whisper(&mut self, neighbour: &Neighbour, nonce: u32, data: String) -> Result<()> {
        let tlv = Tlv::Whisper(crate::payload::tlv::Whisper::new((self.id, neighbour.id, nonce, self.nick.clone() + ":" + &data)));
        self.sender.send((neighbour.host, tlv.clone())).await?;

        self.last_received.push((self.id, nonce));

        self.to_flood.insert((neighbour.host, self.id, nonce), (Duration::from_secs(1), Utc::now().timestamp(), 0, tlv));

        Ok(())
    }

    /// Returns the next neighbours to be timeout
    pub fn next_neighbour_timeout(&self) -> (Duration, Option<SocketAddrV6>) {
        let mut result = (Duration::MAX, None);
        let now = Utc::now();

        for (host, neighbour) in &self.neighbours {
            let timeout = chrono::Duration::minutes(2) - (now - neighbour.last_seen());

            let Ok(timeout) = timeout.to_std() else {
                return (Duration::ZERO, Some(*host));
            };

            if timeout < result.0 {
                result = (timeout, Some(*host));
            }
        }

        result
    }

    /// Sends a Hello TLV to all neighbours
    pub async fn ping_neighbours(&self) -> Result<()> {
        for host in &self.potential {
            if !self.neighbours.contains_key(host) {
                self.sender.send((*host, Tlv::Hello(tlv::Hello::new((self.id, None))))).await?;
            }
        }

        for (host, neighbour) in &self.neighbours {
            self.sender.send((*host, Tlv::Hello(tlv::Hello::new((self.id, Some(neighbour.id)))))).await?;
        }

        Ok(())
    }

    /// Sends neighbour list to all neighbours
    pub async fn send_neighbours(&self) -> Result<()> {
        for (host, neighbour) in &self.neighbours {
            if !neighbour.is_symmetric() {
                continue;
            }

            for neighbour in self.neighbours.values() {
                let data = (IpAddr::V6(*host.ip()), host.port());

                self.sender.send((neighbour.host, Tlv::Neighbour(tlv::Neighbour::new(data)))).await?;
            }
        }

        Ok(())
    }

    /// Checks the list of messages to flood and sends thoses needed
    pub async fn flooding(
        &mut self,
        sender_command: UnboundedSender<Command>,
        sender_data: UnboundedSender<Data>,
        system: Neighbour,
    ) -> Result<()> {
        for ((host, _, _), (duration, begin_time, n, tlv)) in self.to_flood.iter_mut() {
            let new_duration = Duration::from_millis(
                duration
                    .as_millis()
                    .saturating_sub(Utc::now().timestamp_millis().saturating_sub(*begin_time).try_into().unwrap())
                    .try_into()
                    .unwrap(),
            );

            if new_duration.as_millis() == 0 {
                if n <= &mut 4 {
                    sender_data.send(Data {
                        neighbour: system.clone(),
                        content: DataType::System(format!("{}th flood {:?} with {:?}", *n + 1, host, tlv)),
                    })?;

                    self.sender.send((*host, tlv.clone())).await?;
                    begin_time.clone_from(&Utc::now().timestamp_millis());
                    duration.clone_from(&Duration::from_millis(2u64.pow(*n) * ((rand::random::<u64>() % 1000) + 1000)));
                } else {
                    sender_command.send(Command::SendGoAway(*host, 2, "Host did not reply after 5 floods".to_owned()))?;
                    self.neighbours.remove(host);
                    sender_data.send(Data {
                        neighbour: system.clone(),
                        content: DataType::System(format!("Removed {host:?} from the list of neighbours")),
                    })?;
                }
                n.clone_from(&(*n + 1));
            } else {
                duration.clone_from(&new_duration);
            }
        }

        self.to_flood.retain(|_, (_, _, n, _)| n <= &mut 5);

        Ok(())
    }
}
