use chrono::{DateTime, Utc};
use tui::layout::{Margin, Rect};
use tui::style::{Color, Style};
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders, Paragraph, Widget};

use super::input::INPUT_MARGIN;
use crate::server::neighbour::Neighbour;

/// Structure for a chat
///
/// There is exactly :
/// * one chat for the system
/// * one chat with everyone
/// * one chat for each neighbour
#[derive(Debug, Clone)]
pub struct Chat {
    pub neighbour: Neighbour,
    messages: Vec<Message>,
}

pub const CHAT_MARGIN: Margin = Margin {
    horizontal: 1,
    vertical: 1,
};

/// Structure for messages displayed in chats
#[derive(Debug, Default, Clone)]
struct Message {
    id: u64,
    nonce: u32,
    nick: String,
    message: String,
    timestamp: DateTime<Utc>,
    is_received: bool,
}

impl Chat {
    pub fn new(neighbour: Neighbour) -> Self {
        Chat {
            neighbour,
            messages: vec![],
        }
    }

    /// Adds a message to a chat
    pub fn add_message(&mut self, id: u64, nonce: u32, nick: String, message: String, is_received: bool) {
        self.messages.push(Message {
            id,
            nonce,
            nick,
            message,
            timestamp: Utc::now(),
            is_received,
        })
    }

    /// Set the message `(id, nonce)` has received
    pub fn received_message(&mut self, id: u64, nonce: u32) {
        let index = match self.messages.iter().position(|messsage| messsage.id == id && messsage.nonce == nonce) {
            None => return,
            Some(index) => index,
        };
        self.messages.get_mut(index).unwrap().is_received = true;
    }
}

impl<'a> Message {
    fn to_spans(&self, area: Rect) -> Vec<Spans<'a>> {
        [
            vec![Spans::from(vec![
                // The sender color is green
                Span::styled(format!("{}  ", self.nick), Style::default().fg(Color::Green)),
                // The time color is cyan
                Span::styled(self.timestamp.format("%H:%M:%S").to_string(), Style::default().fg(Color::Cyan)),
            ])],
            self.message
                .chars()
                .collect::<Vec<char>>()
                .chunks(usize::from(area.inner(&INPUT_MARGIN).width))
                .map(|chunk| {
                    Spans::from(if self.is_received {
                        Span::raw(chunk.iter().collect::<String>())
                    } else {
                        // Messages that has not been received by anyone are displayed in red
                        Span::styled(chunk.iter().collect::<String>(), Style::default().fg(Color::LightRed))
                    })
                })
                .collect::<Vec<Spans>>(),
            vec![Spans::default()],
        ]
        .concat()
    }
}

impl Widget for &mut Chat {
    fn render(self, area: tui::layout::Rect, buf: &mut tui::buffer::Buffer) {
        let mut message_spans =
            self.messages.iter().map(|message| message.clone().to_spans(area)).collect::<Vec<Vec<Spans>>>().concat();

        Paragraph::new(message_spans.split_off(message_spans.len().saturating_sub(area.inner(&CHAT_MARGIN).height.into())))
            .block(Block::default().borders(Borders::all()).title("MIRC"))
            .render(area, buf)
    }
}
