use tui::layout::Margin;
use tui::text::Spans;
use tui::widgets::{Block, Borders, Paragraph, Widget};
use unicode_segmentation::UnicodeSegmentation;

/// Structure for the user input
#[derive(Debug, Default, Clone)]
pub struct Input {
    pub nick: String,
    pub input: String,
    pub cursor: u16,
}

pub const INPUT_MARGIN: Margin = Margin {
    horizontal: 1,
    vertical: 1,
};

impl Input {
    /// Returns the content of the input and reset it
    pub fn pop_input(&mut self) -> String {
        let input = self.input.clone();
        self.input = "".to_owned();
        input
    }

    /// Deletes the character where the cursor is
    pub fn input_backspace(&mut self) {
        if !self.input.is_empty() {
            let mut chars = self.input.chars();
            chars.next_back();
            self.input = chars.as_str().to_owned()
        }
    }

    /// Inserts the given string where the cursor is
    pub fn input_insert(&mut self, string: String) {
        let mut graphems = self.input.graphemes(true).collect::<Vec<&str>>();
        graphems.insert(self.cursor.into(), &string);
        self.input = graphems.into_iter().collect::<String>()
    }

    /// Returns the length of the input
    pub fn input_len(&self) -> u16 {
        u16::try_from(self.input.len()).unwrap()
    }

    /// Moves the cursor to the left if possible
    pub fn cursor_left(&mut self) {
        self.cursor = if self.cursor > 0 { self.cursor - 1 } else { self.cursor }
    }

    /// Moves the cursor to the right if possible
    pub fn cursor_right(&mut self) {
        self.cursor = if self.cursor < self.input_len() { self.cursor + 1 } else { self.cursor }
    }

    /// Resets the cursor position
    pub fn reset_cursor(&mut self) {
        self.cursor = 0
    }
}

impl Widget for &mut Input {
    fn render(self, area: tui::layout::Rect, buf: &mut tui::buffer::Buffer) {
        let mut text = self
            .input
            .chars()
            .collect::<Vec<char>>()
            .chunks(usize::from(area.inner(&INPUT_MARGIN).width))
            .map(|chunk| Spans::from(chunk.iter().collect::<String>()))
            .collect::<Vec<Spans>>();

        let row_number = text.len();

        Paragraph::new(text.split_off(row_number.saturating_sub(usize::from(area.inner(&INPUT_MARGIN).height))))
            .block(Block::borders(Block::default(), Borders::all()).title(self.nick.clone()))
            .render(area, buf)
    }
}
