use tui::style::{Modifier, Style};
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders, Paragraph, Widget};

use super::chat::Chat;
use crate::server::neighbour::Neighbour;

/// Left part of the TUI
#[derive(Debug, Default)]
pub struct Tabs {
    tabs: Vec<(Neighbour, Chat)>,
    current_tab: usize,
}

impl Tabs {
    /// Goes one tab up if possible
    pub fn tab_up(&mut self) {
        self.current_tab = self.current_tab.saturating_sub(1)
    }

    /// Goes one tab up if possible
    pub fn tab_down(&mut self) {
        self.current_tab = (self.current_tab + 1).min(self.tabs.len() - 1)
    }

    /// Goes to the last tab
    pub fn tab_last(&mut self) {
        self.current_tab = self.tabs.len() - 1
    }

    /// Returns a mutable borrow of the current chat
    pub fn current_chat(&mut self) -> &mut Chat {
        let (_, chat) = self.tabs.get_mut(self.current_tab).unwrap();
        chat
    }

    /// Returns a boolean whether the given neighbour has a tab
    pub fn contains(&self, neighbour: &Neighbour) -> bool {
        self.tabs.iter().map(|(n, _)| n == neighbour).any(|b| b)
    }

    /// Creates a new tab
    pub fn insert(&mut self, neighbour: Neighbour) {
        self.tabs.push((neighbour.clone(), Chat::new(neighbour)))
    }

    /// Set the current tab to the one corresponding to the given neighbour
    pub fn set_tab(&mut self, neighbour: &Neighbour) -> Option<()> {
        self.current_tab = self.tabs.iter().position(|(n, _)| n == neighbour)?;
        Some(())
    }

    /// Return the chat corresponding to the given neighbour
    pub fn get_tab(&mut self, neighbour: &Neighbour) -> Option<&mut Chat> {
        let index = self.tabs.iter().position(|(n, _)| n == neighbour)?;
        self.tabs.get_mut(index).map(|(_, chat)| chat)
    }

    /// Change our nickname
    pub fn set_nick(&mut self, neighbour: &Neighbour, nick: String) -> Option<()> {
        let index = self.tabs.iter().position(|(n, _)| n == neighbour)?;
        if let Some((n, _)) = self.tabs.get_mut(index) {
            n.nick.clone_from(&nick)
        };
        Some(())
    }

    /// Set the message `(id, nonce)` has received
    pub fn received_message(&mut self, id: u64, nonce: u32) {
        self.tabs.iter_mut().for_each(|(_, chat)| chat.received_message(id, nonce))
    }
}

impl Widget for &mut Tabs {
    fn render(self, area: tui::layout::Rect, buf: &mut tui::buffer::Buffer) {
        let current_neighbour = self.current_chat().neighbour.clone();
        Paragraph::new(
            self.tabs
                .iter()
                .map(|(neighbour, _)| {
                    if &current_neighbour == neighbour {
                        // The current tab is displayed with bold characters
                        Spans::from(Span::styled(neighbour.nick.to_owned(), Style::default().add_modifier(Modifier::BOLD)))
                    } else {
                        Spans::from(neighbour.nick.to_owned())
                    }
                })
                .collect::<Vec<Spans>>(),
        )
        .block(Block::default().borders(Borders::all()).title("Tabs"))
        .render(area, buf)
    }
}
