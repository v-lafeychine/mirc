use std::net::IpAddr;

/// Trait indicating that it is possible to obtain the length of its byte representation
pub trait ByteLength {
    /// Return the length of its byte representation
    fn byte_length(&self) -> usize;
}

impl ByteLength for String {
    fn byte_length(&self) -> usize {
        self.as_bytes().len()
    }
}

impl ByteLength for u8 {
    fn byte_length(&self) -> usize {
        1
    }
}

impl ByteLength for u16 {
    fn byte_length(&self) -> usize {
        2
    }
}

impl ByteLength for u32 {
    fn byte_length(&self) -> usize {
        4
    }
}

impl ByteLength for u64 {
    fn byte_length(&self) -> usize {
        8
    }
}

impl ByteLength for IpAddr {
    /// Return the length of its byte representation
    ///
    /// It is supposed here that IP addresses are all represented
    /// on 16 bytes (so all IPv6 or mapped-IPv4).
    fn byte_length(&self) -> usize {
        16
    }
}

impl<T: ByteLength> ByteLength for Vec<T> {
    /// Return the length of its byte representation
    ///
    /// It is supposed here that the representation in a vector
    /// has no influence on the final byte length.
    fn byte_length(&self) -> usize {
        self.iter().fold(0, |acc, tlv| acc + tlv.byte_length())
    }
}

impl<T: ByteLength> ByteLength for Box<T> {
    /// Return the length of its byte representation
    ///
    /// It is supposed here that the byte length wanted is the length of
    /// the content and not the one of the box.
    fn byte_length(&self) -> usize {
        let content = self.as_ref();
        content.byte_length()
    }
}
