//! Serialization and deserialization of structures used in this project
//! have to be towards and from a byte string structure.
//!
//! To respect contraints of the subject, structures, option types, etc
//! have to be serialized without any trace on the final output.
//!
//! All of this is implemented in the structure `ByteString`, which implements
//! `Serializer` and `Deserializer` from serde.

use serde::ser::{
    SerializeMap, SerializeSeq, SerializeStruct, SerializeStructVariant, SerializeTuple, SerializeTupleStruct,
    SerializeTupleVariant,
};
use serde::{Deserializer, Serializer};

use crate::payload::error::Error;

/// Byte string array used for serialization and deserialazation
#[derive(Debug, Default, Clone)]
pub struct ByteString {
    pub array: Vec<u8>,
    current_index: usize,
}

impl Iterator for ByteString {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        match self.array.get(self.current_index) {
            None => None,
            Some(byte) => {
                self.current_index += 1;
                Some(byte.to_owned())
            },
        }
    }
}

impl ByteString {
    /// Create a new byte string containing exactly the bytes given in input
    pub fn new(array: Vec<u8>) -> Self {
        ByteString {
            array,
            current_index: 0,
        }
    }

    /// Return whether self is empty or not
    pub fn is_empty(&self) -> bool {
        self.array.is_empty()
    }

    /// Return the length (the number of bytes) of self
    pub fn len(&self) -> usize {
        self.array.len()
    }

    /// Return the `k` first element of the byte string and remove them from it
    pub fn peek(&mut self, k: usize) -> Result<Vec<u8>, Error> {
        let mut first_elements = vec![];
        if self.array.len() >= k {
            for _ in 0..k {
                first_elements.push(self.array.remove(0));
            }
            Ok(first_elements)
        } else {
            Err(Error::Message("Not enough bytes to deserialize the structure".to_owned()))
        }
    }

    /// Get the content of the byte string
    pub fn get(self) -> Vec<u8> {
        self.array
    }
}

impl<'a> Serializer for &'a mut ByteString {
    type Error = Error;
    type Ok = ();
    type SerializeMap = Self;
    type SerializeSeq = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;

    fn serialize_bool(self, v: bool) -> Result<Self::Ok, Self::Error> {
        self.array.push(u8::from(v));
        Ok(())
    }

    fn serialize_i8(self, v: i8) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        self.array.push(bytes[0]);
        Ok(())
    }

    fn serialize_i16(self, v: i16) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_i32(self, v: i32) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_i64(self, v: i64) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_u8(self, v: u8) -> Result<Self::Ok, Self::Error> {
        self.array.push(v);
        Ok(())
    }

    fn serialize_u16(self, v: u16) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_u32(self, v: u32) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_u64(self, v: u64) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_f32(self, v: f32) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_f64(self, v: f64) -> Result<Self::Ok, Self::Error> {
        let bytes = v.to_be_bytes();
        for byte in bytes {
            self.array.push(byte);
        }
        Ok(())
    }

    fn serialize_char(self, v: char) -> Result<Self::Ok, Self::Error> {
        let string = v.to_string();
        self.serialize_str(&string)
    }

    fn serialize_str(self, v: &str) -> Result<Self::Ok, Self::Error> {
        self.serialize_bytes(v.as_bytes())
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok, Self::Error> {
        for byte in v {
            self.array.push(byte.to_owned());
        }
        Ok(())
    }

    fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }

    fn serialize_some<T: ?Sized>(self, v: &T) -> Result<Self::Ok, Self::Error>
    where
        T: serde::Serialize,
    {
        v.serialize(self)
    }

    fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
        unimplemented!()
    }

    fn serialize_unit_struct(self, _: &'static str) -> Result<Self::Ok, Self::Error> {
        unimplemented!()
    }

    fn serialize_unit_variant(self, _: &'static str, _: u32, _: &'static str) -> Result<Self::Ok, Self::Error> {
        unimplemented!()
    }

    fn serialize_newtype_struct<T: ?Sized>(self, _: &'static str, _: &T) -> Result<Self::Ok, Self::Error>
    where
        T: serde::Serialize,
    {
        unimplemented!()
    }

    fn serialize_newtype_variant<T: ?Sized>(
        self,
        _: &'static str,
        _: u32,
        _: &'static str,
        value: &T,
    ) -> Result<Self::Ok, Self::Error>
    where
        T: serde::Serialize,
    {
        value.serialize(self)
    }

    fn serialize_seq(self, _: Option<usize>) -> Result<Self::SerializeSeq, Self::Error> {
        Ok(self)
    }

    fn serialize_tuple(self, _: usize) -> Result<Self::SerializeTuple, Self::Error> {
        Ok(self)
    }

    fn serialize_tuple_struct(self, _: &'static str, _: usize) -> Result<Self::SerializeTupleStruct, Self::Error> {
        unimplemented!()
    }

    fn serialize_tuple_variant(
        self,
        _: &'static str,
        _: u32,
        _: &'static str,
        _: usize,
    ) -> Result<Self::SerializeTupleVariant, Self::Error> {
        Ok(self)
    }

    fn serialize_map(self, _: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
        unimplemented!()
    }

    fn serialize_struct(self, _: &'static str, _: usize) -> Result<Self::SerializeStruct, Self::Error> {
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _: &'static str,
        _: u32,
        _: &'static str,
        _: usize,
    ) -> Result<Self::SerializeStructVariant, Self::Error> {
        unimplemented!()
    }
}

impl<'a> SerializeSeq for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }
}

impl<'a> SerializeTuple for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }
}

impl<'a> SerializeTupleStruct for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_field<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        unimplemented!()
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        unimplemented!()
    }
}

impl<'a> SerializeTupleVariant for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }
}

impl<'a> SerializeMap for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_key<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        unimplemented!()
    }

    fn serialize_value<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        unimplemented!()
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        unimplemented!()
    }
}

impl<'a> SerializeStruct for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_field<T: ?Sized>(&mut self, _: &'static str, value: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }
}

impl<'a> SerializeStructVariant for &'a mut ByteString {
    type Error = Error;
    type Ok = ();

    fn serialize_field<T: ?Sized>(&mut self, _: &'static str, _: &T) -> Result<(), Self::Error>
    where
        T: serde::Serialize,
    {
        unimplemented!()
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        unimplemented!()
    }
}

impl<'de, 'a> Deserializer<'de> for &'a mut ByteString {
    type Error = Error;

    fn deserialize_any<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        let byte = self.peek(1)?[0];
        visitor.visit_bool(byte != 0)
    }

    fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        let byte = self.peek(1)?[0];
        visitor.visit_i8(i8::from_be_bytes([byte]))
    }

    fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 2;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_i16(i16::from_be_bytes(be_bytes))
    }

    fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 4;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_i32(i32::from_be_bytes(be_bytes))
    }

    fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 8;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_i64(i64::from_be_bytes(be_bytes))
    }

    fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        let byte = self.peek(1)?[0];
        visitor.visit_u8(byte)
    }

    fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 2;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_u16(u16::from_be_bytes(be_bytes))
    }

    fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 4;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_u32(u32::from_be_bytes(be_bytes))
    }

    fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 8;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_u64(u64::from_be_bytes(be_bytes))
    }

    fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 4;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_f32(f32::from_be_bytes(be_bytes))
    }

    fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        const NB_BYTES: usize = 8;
        let bytes = self.peek(NB_BYTES)?;
        let mut be_bytes = [0; NB_BYTES];
        be_bytes.copy_from_slice(&bytes[..NB_BYTES]);
        visitor.visit_f64(f64::from_be_bytes(be_bytes))
    }

    fn deserialize_char<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        self.deserialize_string(visitor)
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        visitor.visit_string(match String::from_utf8(self.array.clone()) {
            Ok(string) => string,
            Err(_) => return Err(Error::Message("Cannot deserialize bytes into a UTF-8 text".to_owned())),
        })
    }

    fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        visitor.visit_bytes(&self.array)
    }

    fn deserialize_byte_buf<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_option<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit_struct<V>(self, _: &'static str, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_newtype_struct<V>(self, _: &'static str, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        visitor.visit_bytes(&self.array)
    }

    fn deserialize_tuple<V>(self, _: usize, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_tuple_struct<V>(self, _: &'static str, _: usize, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_map<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_struct<V>(self, _: &'static str, _: &'static [&'static str], visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        visitor.visit_bytes(&self.array)
    }

    fn deserialize_enum<V>(self, _: &'static str, _: &'static [&'static str], visitor: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        visitor.visit_bytes(&self.array)
    }

    fn deserialize_identifier<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_ignored_any<V>(self, _: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::Visitor<'de>,
    {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use serde::ser::SerializeTuple;
    use serde::{Serialize, Serializer};

    use super::ByteString;

    #[test]
    fn serialize_bool() {
        let mut serializer = ByteString::default();
        assert_eq!(true.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.array, vec![1]);
        assert_eq!(false.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.array, vec![1, 0]);
    }

    #[test]
    fn serialize_u8() {
        let mut serializer = ByteString::default();
        assert_eq!(12u8.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![12]);
    }

    #[test]
    fn serialize_u16() {
        let mut serializer = ByteString::default();
        assert_eq!(12u16.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![0, 12]);

        serializer = ByteString::default();
        assert_eq!(19122u16.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![74, 178]);
    }

    #[test]
    fn serialize_u64() {
        let mut serializer = ByteString::default();
        assert_eq!(901590660296433528u64.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![12, 131, 23, 237, 191, 199, 95, 120]);
    }

    #[test]
    fn serialize_char() {
        let mut serializer = ByteString::default();
        assert_eq!('a'.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![97]);

        serializer = ByteString::default();
        assert_eq!('Ø'.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![195, 152]);
    }

    #[test]
    fn serialize_string() {
        let mut serializer = ByteString::default();
        assert_eq!("aØ".serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![97, 195, 152]);
    }

    #[test]
    fn serialize_bytes() {
        let mut serializer = ByteString::default();
        assert_eq!(([152, 83, 97] as [u8; 3]).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![152, 83, 97]);
    }

    #[test]
    fn serialize_tuple() {
        let mut serializer = ByteString::default();
        match serializer.serialize_tuple(3) {
            Ok(s) => {
                let mut serializer = s.to_owned();
                assert_eq!(true.serialize(&mut serializer), Ok(()));
                assert_eq!(19122u16.serialize(&mut serializer), Ok(()));
                assert_eq!('Ø'.serialize(&mut serializer), Ok(()));
                assert_eq!(serializer.end(), Ok(()));
                assert_eq!(serializer.get(), vec![1, 74, 178, 195, 152]);
            },
            Err(_) => panic!("Could not start serializing tuple"),
        }
    }
}

#[cfg(test)]
mod byte_string_struct {
    use super::ByteString;

    #[test]
    fn len() {
        let mut byte_string = ByteString::new(vec![0, 1, 2, 3, 4]);
        assert_eq!(byte_string.len(), 5);

        byte_string = ByteString::new(vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(byte_string.len(), 10);
    }

    #[test]
    fn peek() {
        let mut byte_string = ByteString::new(vec![0, 1, 2, 3, 4]);
        assert_eq!(byte_string.peek(3), Ok(vec![0, 1, 2]));
        assert_eq!(byte_string.get(), vec![3, 4]);

        byte_string = ByteString::new(vec![0, 1, 2, 3, 4]);
        assert!(byte_string.peek(10).is_err());
    }

    #[test]
    fn is_empty() {
        let mut byte_string = ByteString::new(vec![0, 1, 2, 3, 4]);
        assert!(!byte_string.is_empty());

        byte_string = ByteString::new(vec![]);
        assert!(byte_string.is_empty());
    }

    #[test]
    fn get() {
        let byte_string = ByteString::new(vec![0, 1, 2, 3, 4]);
        assert_eq!(byte_string.get(), vec![0, 1, 2, 3, 4]);
    }
}
