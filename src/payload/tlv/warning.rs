use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::{StringVisitor, U8Visitor};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Warning {
    pub message: String,
}

#[derive(Default, Debug)]
pub struct WarningVisitor {}

impl ByteLength for Warning {
    fn byte_length(&self) -> usize {
        2 + self.message.byte_length()
    }
}

impl Serialize for Warning {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Warning", 3)?;
        s.serialize_field("tag", &self.tag())?;
        match self.length() {
            None => return Err(serde::ser::Error::custom("Cannot cast Tlv body length into a 8-bit integer")),
            Some(length) => s.serialize_field("length", &length)?,
        };
        s.serialize_field("message", &self.message)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Warning {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("Warning", &["tag", "length", "message"], WarningVisitor::default())
    }
}

impl Tlv for Warning {
    fn tag(&self) -> u8 {
        7
    }

    fn length(&self) -> Option<u8> {
        match u8::try_from(self.message.byte_length()) {
            Ok(length) => Some(length),
            Err(_) => None,
        }
    }

    fn value(&self) -> Option<Vec<u8>> {
        Some(self.message.clone().into_bytes())
    }
}

impl Instantiable for Warning {
    type Requirements = String;

    /// Creates a Warning TLV from the message
    ///
    /// ```
    /// fn new(message: String) -> Warning
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        Warning {
            message: requirements,
        }
    }
}

impl<'de> Visitor<'de> for WarningVisitor {
    type Value = Warning;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "Warning TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let length = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;

        let mut data_bytes = ByteString::new(convert_result(byte_string.peek(usize::from(length)))?);
        let message = convert_result(data_bytes.deserialize_string(StringVisitor {}))?;
        Ok(Warning::new(message))
    }
}
