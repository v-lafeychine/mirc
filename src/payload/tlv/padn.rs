use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::U8Visitor;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct PadN {
    length: u8,
}

#[derive(Default, Debug)]
pub struct PadNVisitor {}

impl ByteLength for PadN {
    fn byte_length(&self) -> usize {
        usize::from(self.length) + 2
    }
}

impl Serialize for PadN {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("PadN", (self.length + 2).into())?;
        s.serialize_field("tag", &self.tag())?;
        s.serialize_field("length", match &self.length() {
            None => unreachable!(),
            Some(length) => length,
        })?;
        for _ in 0..self.length.to_owned() {
            s.serialize_field("value", &0u8)?;
        }
        s.end()
    }
}

impl<'de> Deserialize<'de> for PadN {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("PadN", &["tag", "length", "value"], PadNVisitor::default())
    }
}

impl Tlv for PadN {
    fn tag(&self) -> u8 {
        1
    }

    fn length(&self) -> Option<u8> {
        Some(self.length)
    }

    fn value(&self) -> Option<Vec<u8>> {
        Some(Vec::from_iter(std::iter::repeat(0u8).take(usize::from(self.length()?))))
    }
}

impl Instantiable for PadN {
    type Requirements = u8;

    /// Creates a PadN TLV from its length
    ///
    ///
    /// ```
    /// fn new(length: u8) -> Hello
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        PadN {
            length: requirements,
        }
    }
}

impl<'de> Visitor<'de> for PadNVisitor {
    type Value = PadN;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "PadN TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let length = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        for _ in 0..length {
            convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        }

        Ok(PadN::new(length))
    }
}
