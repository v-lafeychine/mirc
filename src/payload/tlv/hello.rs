use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::{U64Visitor, U8Visitor};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Hello {
    pub source_id: u64,
    pub destination_id: Option<u64>,
}

#[derive(Default, Debug)]
pub struct HelloVisitor {}

impl ByteLength for Hello {
    fn byte_length(&self) -> usize {
        10 + if self.destination_id.is_some() { 8 } else { 0 }
    }
}

impl Serialize for Hello {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Hello", 4)?;
        s.serialize_field("tag", &self.tag())?;
        match self.length() {
            None => return Err(serde::ser::Error::custom("Cannot cast Tlv body length into a 8-bit integer")),
            Some(length) => s.serialize_field("length", &length)?,
        };
        s.serialize_field("source_id", &self.source_id)?;
        s.serialize_field("destination_id", &self.destination_id)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Hello {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("Hello", &["tag", "length", "source_id", "destination_id"], HelloVisitor::default())
    }
}

impl Tlv for Hello {
    fn tag(&self) -> u8 {
        2
    }

    fn length(&self) -> Option<u8> {
        Some(if self.destination_id.is_some() { 16 } else { 8 })
    }

    fn value(&self) -> Option<Vec<u8>> {
        match self.destination_id {
            None => Some(self.source_id.to_be_bytes().to_vec()),
            Some(dest_id) => Some([self.source_id.to_be_bytes().to_vec(), dest_id.to_be_bytes().to_vec()].concat()),
        }
    }
}

impl Instantiable for Hello {
    type Requirements = (u64, Option<u64>);

    /// Creates a Hello TLV from the source ID, and the destination ID if present
    ///
    /// ```
    /// fn new((source_id, destination_id): (u64, Option<u64>)) -> Hello
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        let (source_id, destination_id) = requirements;
        Hello {
            source_id,
            destination_id,
        }
    }
}

impl<'de> Visitor<'de> for HelloVisitor {
    type Value = Hello;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "Hello TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let length = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;

        match length {
            8 => {
                let source_id = convert_result(byte_string.deserialize_u64(U64Visitor {}))?;
                Ok(Hello::new((source_id, None)))
            },
            16 => {
                let source_id = convert_result(byte_string.deserialize_u64(U64Visitor {}))?;
                let destination_id = convert_result(byte_string.deserialize_u64(U64Visitor {}))?;
                Ok(Hello::new((source_id, Some(destination_id))))
            },
            _ => Err(E::custom("Unusual Hello TLV structure")),
        }
    }
}
