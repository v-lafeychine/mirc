use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::{StringVisitor, U8Visitor};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GoAway {
    pub code: u8,
    pub message: String,
}

#[derive(Default, Debug)]
pub struct GoAwayVisitor {}

impl ByteLength for GoAway {
    fn byte_length(&self) -> usize {
        3 + self.message.byte_length()
    }
}

impl Serialize for GoAway {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("GoAway", 4)?;
        s.serialize_field("tag", &self.tag())?;
        match self.length() {
            None => return Err(serde::ser::Error::custom("Cannot cast Tlv body length into a 8-bit integer")),
            Some(length) => s.serialize_field("length", &length)?,
        };
        s.serialize_field("code", &self.code)?;
        s.serialize_field("message", &self.message)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for GoAway {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("GoAway", &["tag", "length", "code", "message"], GoAwayVisitor::default())
    }
}

impl Tlv for GoAway {
    fn tag(&self) -> u8 {
        6
    }

    fn length(&self) -> Option<u8> {
        match u8::try_from(self.code.byte_length() + self.message.byte_length()) {
            Ok(length) => Some(length),
            Err(_) => None,
        }
    }

    fn value(&self) -> Option<Vec<u8>> {
        Some([self.code.to_be_bytes().to_vec(), self.message.clone().into_bytes()].concat())
    }
}

impl Instantiable for GoAway {
    type Requirements = (u8, String);

    /// Creates a GoAway TLV from the code and the message
    ///
    /// ```
    /// fn new((code, message): (u8, String)) -> GoAway
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        let (code, message) = requirements;
        GoAway { code, message }
    }
}

impl<'de> Visitor<'de> for GoAwayVisitor {
    type Value = GoAway;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "GoAway TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let length = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;

        let mut data_bytes = ByteString::new(convert_result(byte_string.peek(usize::from(length)))?);
        let code = convert_result(data_bytes.deserialize_u8(U8Visitor {}))?;
        let message = convert_result(data_bytes.deserialize_string(StringVisitor {}))?;
        Ok(GoAway::new((code, message)))
    }
}
