pub mod ack;
pub mod data;
pub mod go_away;
pub mod hello;
pub mod neighbour;
pub mod other;
pub mod pad1;
pub mod padn;
pub mod warning;
pub mod whisper;

use std::fmt::Debug;

use erased_serde::{serialize_trait_object, Serialize};

use crate::payload::byte_length::ByteLength;
pub use crate::payload::tlv::ack::Ack;
pub use crate::payload::tlv::data::Data;
pub use crate::payload::tlv::go_away::GoAway;
pub use crate::payload::tlv::hello::Hello;
pub use crate::payload::tlv::neighbour::Neighbour;
pub use crate::payload::tlv::other::Other;
pub use crate::payload::tlv::pad1::Pad1;
pub use crate::payload::tlv::padn::PadN;
pub use crate::payload::tlv::warning::Warning;
pub use crate::payload::tlv::whisper::Whisper;

/// A TLV is a triplet `(tag, length, value)`
///
/// * The `tag` is an integer between 0 and 7 which indicates the type of the TLV
/// * The `length` is the length of value in bytes
/// * The `value` is the content of TLV
pub trait Tlv: Serialize + ByteLength {
    /// Return the tag of the Tlv
    fn tag(&self) -> u8;

    /// Return the length of the value in bytes if it exists
    fn length(&self) -> Option<u8>;

    /// Return the value, which is a list of bytes
    fn value(&self) -> Option<Vec<u8>>;
}

/// Generic trait for TLV to make easier their creation
pub trait Instantiable {
    /// Type required to create a new instance of Self
    type Requirements;

    /// Create a new TLV object from its length and its value
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized;
}

impl Debug for Box<dyn Tlv> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Tag: {:?}\nLength: {:?}\nValue: {:?}", self.tag(), self.length(), self.value())
    }
}

impl PartialEq for Box<dyn Tlv> {
    fn eq(&self, other: &Self) -> bool {
        self.tag() == other.tag() && self.length() == other.length() && self.value() == other.value()
    }
}

impl Eq for Box<dyn Tlv> {}

impl ByteLength for Box<dyn Tlv> {
    fn byte_length(&self) -> usize {
        let tlv = self.as_ref();
        tlv.byte_length()
    }
}

// Macro from erased_serde allowing to serialize automatically Box<dyn Tlv> because Tlv: Serialize
serialize_trait_object!(Tlv);
