use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::U8Visitor;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Other {
    pub tag: u8,
    pub length: u8,
    pub value: Vec<u8>,
}

#[derive(Default, Debug)]
pub struct OtherVisitor {}

impl ByteLength for Other {
    fn byte_length(&self) -> usize {
        2 + self.value.byte_length()
    }
}

impl Serialize for Other {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Other", 3)?;
        s.serialize_field("tag", &self.tag())?;
        match self.length() {
            None => return Err(serde::ser::Error::custom("Cannot cast Tlv body length into a 8-bit integer")),
            Some(length) => s.serialize_field("length", &length)?,
        };
        s.serialize_field("value", &self.value)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Other {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("Other", &["tag", "length", "value"], OtherVisitor::default())
    }
}

impl Tlv for Other {
    fn tag(&self) -> u8 {
        self.tag
    }

    fn length(&self) -> Option<u8> {
        Some(self.length)
    }

    fn value(&self) -> Option<Vec<u8>> {
        Some(self.value.clone())
    }
}

impl Instantiable for Other {
    type Requirements = (u8, u8, Vec<u8>);

    /// Creates an Other TLV from the tag, the length and the value
    ///
    /// ```
    /// fn new((tag, length, value): (u8, u8, Vec<u8>)) -> Other
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        let (tag, length, value) = requirements;
        Other { tag, length, value }
    }
}

impl<'de> Visitor<'de> for OtherVisitor {
    type Value = Other;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "Other TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let tag = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let length = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;

        let value = ByteString::new(convert_result(byte_string.peek(usize::from(length)))?);
        Ok(Other::new((tag, length, value.get())))
    }
}
