use std::net::{IpAddr, Ipv4Addr};

use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::{U16Visitor, U8Visitor};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Neighbour {
    pub addr: IpAddr,
    pub port: u16,
}

#[derive(Default, Debug)]
pub struct NeighbourVisitor {}

impl ByteLength for Neighbour {
    fn byte_length(&self) -> usize {
        20
    }
}

impl Serialize for Neighbour {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Neighbour", 4)?;
        s.serialize_field("tag", &self.tag())?;
        match self.length() {
            None => return Err(serde::ser::Error::custom("Cannot cast Tlv body length into a 8-bit integer")),
            Some(length) => s.serialize_field("length", &length)?,
        };
        s.serialize_field("addr", &to_v6_canonical(&self.addr))?;
        s.serialize_field("port", &self.port)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Neighbour {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("Neighbour", &["tag", "length", "source_id", "destination_id"], NeighbourVisitor::default())
    }
}

impl Tlv for Neighbour {
    fn tag(&self) -> u8 {
        3
    }

    fn length(&self) -> Option<u8> {
        Some(18)
    }

    fn value(&self) -> Option<Vec<u8>> {
        Some([to_v6_canonical(&self.addr).to_vec(), self.port.to_be_bytes().to_vec()].concat())
    }
}

impl Instantiable for Neighbour {
    type Requirements = (IpAddr, u16);

    /// Creates a Neighbour TLV from the IP address and the port
    ///
    /// ```
    /// fn new((addr, port): (IpAddr, u16)) -> Neighbour
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        let (addr, port) = requirements;
        Neighbour { addr, port }
    }
}

impl<'de> Visitor<'de> for NeighbourVisitor {
    type Value = Neighbour;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "Neighbour TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;

        let mut addr: [u8; 16] = [0u8; 16];
        for byte in &mut addr {
            byte.clone_from(&convert_result(byte_string.deserialize_u8(U8Visitor {}))?);
        }
        let port = convert_result(byte_string.deserialize_u16(U16Visitor {}))?;
        Ok(Neighbour::new((from_v6_canonical(IpAddr::from(addr)), port)))
    }
}

/// Converts an IP address to 128 bits
///
/// If it is an IPv4 address, it is converted to a IPv4-mapped address beforehand
fn to_v6_canonical(addr: &IpAddr) -> [u8; 16] {
    match addr {
        IpAddr::V4(ip) => ip.to_ipv6_mapped().octets(),
        IpAddr::V6(ip) => ip.octets(),
    }
}

/// Converts an IP address to an IPv6 address
///
/// If the input is already an IPv6 address, does nothing.
///
/// If the input is an IPv4, converts it to a mapped IPv6 address.
fn from_v6_canonical(addr: IpAddr) -> IpAddr {
    match addr {
        IpAddr::V4(ip) => IpAddr::V4(ip),
        IpAddr::V6(ip) => match ip.octets() {
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, a, b, c, d] => IpAddr::V4(Ipv4Addr::new(a, b, c, d)),
            _ => IpAddr::V6(ip),
        },
    }
}
