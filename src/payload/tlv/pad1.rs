use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::U8Visitor;

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Pad1;

#[derive(Default)]
pub struct Pad1Visitor {}

impl ByteLength for Pad1 {
    fn byte_length(&self) -> usize {
        1
    }
}

impl Serialize for Pad1 {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Pad1", 1)?;
        s.serialize_field("tag", &self.tag())?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Pad1 {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("Pad1", &["tag"], Pad1Visitor::default())
    }
}

impl Tlv for Pad1 {
    fn tag(&self) -> u8 {
        0
    }

    fn length(&self) -> Option<u8> {
        None
    }

    fn value(&self) -> Option<Vec<u8>> {
        None
    }
}

impl Instantiable for Pad1 {
    type Requirements = ();

    /// Creates a Pad1 TLV
    ///
    ///
    /// ```
    /// fn new(_: ()) -> Hello
    /// ```
    fn new(_: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        Pad1::default()
    }
}

impl<'de> Visitor<'de> for Pad1Visitor {
    type Value = Pad1;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "Pad1 TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        Ok(Pad1 {})
    }
}
