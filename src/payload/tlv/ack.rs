use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use super::{Instantiable, Tlv};
use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::{U32Visitor, U64Visitor, U8Visitor};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Ack {
    pub sender_id: u64,
    pub nonce: u32,
}

#[derive(Default, Debug)]
pub struct AckVisitor {}

impl ByteLength for Ack {
    fn byte_length(&self) -> usize {
        14
    }
}

impl Serialize for Ack {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Ack", 5)?;
        s.serialize_field("tag", &self.tag())?;
        match self.length() {
            None => return Err(serde::ser::Error::custom("Cannot cast Tlv body length into a 8-bit integer")),
            Some(length) => s.serialize_field("length", &length)?,
        };
        s.serialize_field("sender_id", &self.sender_id)?;
        s.serialize_field("nonce", &self.nonce)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Ack {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_struct("Ack", &["tag", "length", "sender_id", "nonce"], AckVisitor::default())
    }
}

impl Tlv for Ack {
    fn tag(&self) -> u8 {
        5
    }

    fn length(&self) -> Option<u8> {
        match u8::try_from(self.sender_id.byte_length() + self.nonce.byte_length()) {
            Ok(length) => Some(length),
            Err(_) => None,
        }
    }

    fn value(&self) -> Option<Vec<u8>> {
        Some([self.sender_id.to_be_bytes().to_vec(), self.nonce.to_be_bytes().to_vec()].concat())
    }
}

impl Instantiable for Ack {
    type Requirements = (u64, u32);

    /// Creates a Ack TLV from the sender ID and the nonce
    ///
    /// ```
    /// fn new((sender_id, nonce): (u64, u32)) -> Ack
    /// ```
    fn new(requirements: Self::Requirements) -> Self
    where
        Self: Sized,
    {
        let (sender_id, nonce) = requirements;
        Ack { sender_id, nonce }
    }
}

impl<'de> Visitor<'de> for AckVisitor {
    type Value = Ack;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "Ack TLV structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let _ = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;

        let sender_id = convert_result(byte_string.deserialize_u64(U64Visitor {}))?;
        let nonce = convert_result(byte_string.deserialize_u32(U32Visitor {}))?;
        Ok(Ack::new((sender_id, nonce)))
    }
}
