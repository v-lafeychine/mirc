use serde::de::Visitor;
use serde::ser::SerializeStruct;
use serde::{Deserialize, Deserializer, Serialize};

use crate::payload::byte_length::ByteLength;
use crate::payload::error::Error;
use crate::payload::serializer::ByteString;
use crate::payload::visitor::{U16Visitor, U8Visitor};
use crate::payload::Tlv;

/// MIRC message structure
#[derive(Debug, PartialEq, Eq)]
pub struct Message {
    /// Magic number
    pub magic: u8,

    /// Protocol version
    pub version: u8,

    /// List of Tlv
    pub body: Vec<Tlv>,
}

#[derive(Default)]
pub struct MessageVisitor {}

impl ByteLength for Message {
    fn byte_length(&self) -> usize {
        4 + self.body.byte_length()
    }
}

impl Default for Message {
    fn default() -> Self {
        Message {
            magic: 95,
            version: 0,
            body: vec![],
        }
    }
}

impl Message {
    /// Creates a new message from the triplet (magic, version, body)
    pub fn new(magic: u8, version: u8, body: Vec<Tlv>) -> Self {
        let mut message = Message {
            magic,
            version,
            body: vec![],
        };

        body.into_iter().for_each(|tlv| message.add_tlv(tlv));
        message
    }

    /// Add a tlv to an existing message
    pub fn add_tlv(&mut self, tlv: Tlv) {
        self.body.push(tlv);
    }
}

impl<'de> Visitor<'de> for MessageVisitor {
    type Value = Message;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "message structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(element) => Ok(element),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());
        let magic = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let version = convert_result(byte_string.deserialize_u8(U8Visitor {}))?;
        let body_length = convert_result(byte_string.deserialize_u16(U16Visitor {}))?;
        let mut body_bytes = ByteString::new(convert_result(byte_string.peek(usize::from(body_length)))?);

        let mut message = Message::new(magic, version, vec![]);
        while !body_bytes.is_empty() {
            let tlv = convert_result(Tlv::deserialize(&mut body_bytes))?;
            convert_result(body_bytes.peek(tlv.byte_length()))?;
            message.add_tlv(tlv);
        }
        Ok(message)
    }
}

impl Serialize for Message {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Message", 4)?;
        s.serialize_field("magic", &self.magic)?;
        s.serialize_field("version", &self.version)?;
        match u16::try_from(self.body.byte_length()) {
            Ok(length) => s.serialize_field("body_length", &length)?,
            Err(_) => return Err(serde::ser::Error::custom("Cannot cast body length into a 16-bit integer")),
        };
        s.serialize_field("body", &self.body)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Message {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_struct("Message", &["magic", "version", "body_length", "body"], MessageVisitor::default())
    }
}

#[cfg(test)]
mod message_byte_serialization {
    use serde::Serialize;

    use crate::payload::message::Message;
    use crate::payload::serializer::ByteString;
    use crate::payload::tlv::data::Data;
    use crate::payload::tlv::hello::Hello;
    use crate::payload::tlv::pad1::Pad1;
    use crate::payload::tlv::Instantiable;
    use crate::payload::Tlv;

    #[test]
    fn serialize_pad1_message() {
        let mut serializer = ByteString::default();
        let mut message = Message::default();
        message.add_tlv(Tlv::Pad1(Pad1::new(())));

        assert_eq!(message.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![95, 0, 0, 1, 0]);
    }

    #[test]
    fn serialize_hello_message() {
        let mut serializer = ByteString::default();
        let mut message = Message::default();
        message.add_tlv(Tlv::Hello(Hello::new((901590660296433528, Some(12)))));

        assert_eq!(message.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![95, 0, 0, 18, 2, 16, 12, 131, 23, 237, 191, 199, 95, 120, 0, 0, 0, 0, 0, 0, 0, 12]);
    }

    #[test]
    fn serialize_neighbour_data_message() {
        let mut serializer = ByteString::default();
        let mut message = Message::default();
        message.add_tlv(Tlv::Hello(Hello::new((901590660296433528, None))));
        message.add_tlv(Tlv::Data(Data::new((901590660296433528, 123, "test: H3llØ wörld!".to_owned()))));

        assert_eq!(message.serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![
            95, 0, 0, 44, 2, 8, 12, 131, 23, 237, 191, 199, 95, 120, 4, 32, 12, 131, 23, 237, 191, 199, 95, 120, 0, 0, 0, 123, 116,
            101, 115, 116, 58, 32, 72, 51, 108, 108, 195, 152, 32, 119, 195, 182, 114, 108, 100, 33
        ]);
    }
}

#[cfg(test)]
mod message_byte_deserialization {
    use serde::Deserialize;

    use super::Message;
    use crate::payload::serializer::ByteString;
    use crate::payload::tlv::data::Data;
    use crate::payload::tlv::hello::Hello;
    use crate::payload::tlv::pad1::Pad1;
    use crate::payload::tlv::Instantiable;
    use crate::payload::Tlv;

    #[test]
    fn deserialize_pad1_message() {
        let mut serialized_message = ByteString::new(vec![95, 0, 0, 1, 0]);

        let mut message = Message::default();
        message.add_tlv(Tlv::Pad1(Pad1::new(())));

        assert_eq!(Message::deserialize(&mut serialized_message), Ok(message));
    }

    #[test]
    fn deserialize_hello_message() {
        let mut serialized_message =
            ByteString::new(vec![95, 0, 0, 18, 2, 16, 12, 131, 23, 237, 191, 199, 95, 120, 0, 0, 0, 0, 0, 0, 0, 12]);

        let mut message = Message::default();
        message.add_tlv(Tlv::Hello(Hello::new((901590660296433528, Some(12)))));

        assert_eq!(Message::deserialize(&mut serialized_message), Ok(message));
    }

    #[test]
    fn deserialize_neighbour_data_message() {
        let mut serialized_message = ByteString::new(vec![
            95, 0, 0, 44, 2, 8, 12, 131, 23, 237, 191, 199, 95, 120, 4, 32, 12, 131, 23, 237, 191, 199, 95, 120, 0, 0, 0, 123, 116,
            101, 115, 116, 58, 32, 72, 51, 108, 108, 195, 152, 32, 119, 195, 182, 114, 108, 100, 33,
        ]);

        let mut message = Message::default();
        message.add_tlv(Tlv::Hello(Hello::new((901590660296433528, None))));
        message.add_tlv(Tlv::Data(Data::new((901590660296433528, 123, "test: H3llØ wörld!".to_owned()))));

        assert_eq!(Message::deserialize(&mut serialized_message), Ok(message));
    }
}
