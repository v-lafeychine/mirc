use std::fmt::Display;

use derive_more::Display;
use serde::{de, ser};

/// Error type used during the serialization and deserialization
#[derive(Debug, Display, PartialEq, Eq)]
pub enum Error {
    #[display(fmt = "Error : {_0}")]
    Message(String),
}

impl std::error::Error for Error {}

impl ser::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: std::fmt::Display,
    {
        Error::Message(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::Message(msg.to_string())
    }
}
