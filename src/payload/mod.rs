mod byte_length;
mod error;
pub mod message;
pub mod serializer;
pub mod tlv;
mod visitor;

use serde::de::Visitor;
use serde::{Deserialize, Serialize};

use self::byte_length::ByteLength;
use self::error::Error;
use self::serializer::ByteString;

#[derive(Clone, Debug, PartialEq, Eq, Serialize)]
pub enum Tlv {
    Pad1(tlv::pad1::Pad1),
    PadN(tlv::padn::PadN),
    Hello(tlv::hello::Hello),
    Neighbour(tlv::neighbour::Neighbour),
    Data(tlv::data::Data),
    Ack(tlv::ack::Ack),
    GoAway(tlv::go_away::GoAway),
    Warning(tlv::warning::Warning),
    Whisper(tlv::whisper::Whisper),
    Other(tlv::other::Other),
}

impl ByteLength for Tlv {
    fn byte_length(&self) -> usize {
        match self {
            Tlv::Pad1(tlv) => tlv.byte_length(),
            Tlv::PadN(tlv) => tlv.byte_length(),
            Tlv::Hello(tlv) => tlv.byte_length(),
            Tlv::Neighbour(tlv) => tlv.byte_length(),
            Tlv::Data(tlv) => tlv.byte_length(),
            Tlv::Ack(tlv) => tlv.byte_length(),
            Tlv::GoAway(tlv) => tlv.byte_length(),
            Tlv::Warning(tlv) => tlv.byte_length(),
            Tlv::Whisper(tlv) => tlv.byte_length(),
            Tlv::Other(tlv) => tlv.byte_length(),
        }
    }
}

/// Visitor that convert any well-formed byte string to the corresponding TLV
#[derive(Default)]
struct GenericTlvVisitor {}

impl<'de> Visitor<'de> for GenericTlvVisitor {
    type Value = Tlv;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a well-formed byte-Tlv structure")
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        fn convert_result<T, E: serde::de::Error>(result: Result<T, Error>) -> Result<T, E> {
            match result {
                Ok(t) => Ok(t),
                Err(err) => Err(E::custom(err)),
            }
        }

        let mut byte_string = ByteString::new(v.to_vec());

        let tag = match byte_string.next() {
            None => return Err(E::custom("Tried to deserialize an empty TLV")),
            Some(tag) => tag,
        };

        match tag {
            0 => Ok(Tlv::Pad1(convert_result(crate::payload::tlv::Pad1::deserialize(&mut byte_string))?)),
            1 => Ok(Tlv::PadN(convert_result(crate::payload::tlv::PadN::deserialize(&mut byte_string))?)),
            2 => Ok(Tlv::Hello(convert_result(crate::payload::tlv::Hello::deserialize(&mut byte_string))?)),
            3 => Ok(Tlv::Neighbour(convert_result(crate::payload::tlv::Neighbour::deserialize(&mut byte_string))?)),
            4 => Ok(Tlv::Data(convert_result(crate::payload::tlv::Data::deserialize(&mut byte_string))?)),
            5 => Ok(Tlv::Ack(convert_result(crate::payload::tlv::Ack::deserialize(&mut byte_string))?)),
            6 => Ok(Tlv::GoAway(convert_result(crate::payload::tlv::GoAway::deserialize(&mut byte_string))?)),
            7 => Ok(Tlv::Warning(convert_result(crate::payload::tlv::Warning::deserialize(&mut byte_string))?)),
            8 => Ok(Tlv::Whisper(convert_result(crate::payload::tlv::Whisper::deserialize(&mut byte_string))?)),
            _ => Ok(Tlv::Other(convert_result(crate::payload::tlv::Other::deserialize(&mut byte_string))?)),
        }
    }
}

impl<'de> Deserialize<'de> for Tlv {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_bytes(GenericTlvVisitor::default())
    }
}

#[cfg(test)]
mod tlv_byte_serialization {
    use std::net::IpAddr;

    use serde::Serialize;

    use crate::payload::serializer::ByteString;
    use crate::payload::tlv::ack::Ack;
    use crate::payload::tlv::data::Data;
    use crate::payload::tlv::go_away::GoAway;
    use crate::payload::tlv::hello::Hello;
    use crate::payload::tlv::neighbour::Neighbour;
    use crate::payload::tlv::other::Other;
    use crate::payload::tlv::pad1::Pad1;
    use crate::payload::tlv::padn::PadN;
    use crate::payload::tlv::warning::Warning;
    use crate::payload::tlv::{Instantiable, Whisper};

    #[test]
    fn serialize_tlv_pad1() {
        let mut serializer = ByteString::default();
        assert_eq!(Pad1::new(()).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![0]);
    }

    #[test]
    fn serialize_tlv_padn() {
        let mut serializer = ByteString::default();
        assert_eq!(PadN::new(3).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![1, 3, 0, 0, 0]);

        serializer = ByteString::default();
        assert_eq!(PadN::new(5).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![1, 5, 0, 0, 0, 0, 0]);
    }

    #[test]
    fn serialize_tlv_short_hello() {
        let mut serializer = ByteString::default();
        assert_eq!(Hello::new((901590660296433528, None)).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![2, 8, 12, 131, 23, 237, 191, 199, 95, 120]);
    }

    #[test]
    fn serialize_tlv_long_hello() {
        let mut serializer = ByteString::default();
        assert_eq!(Hello::new((901590660296433528, Some(12))).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![2, 16, 12, 131, 23, 237, 191, 199, 95, 120, 0, 0, 0, 0, 0, 0, 0, 12]);
    }

    #[test]
    fn serialize_tlv_neighbour() {
        // Test on the IPv4 address 192.168.1.1:22
        let mut serializer = ByteString::default();
        assert_eq!(Neighbour::new((IpAddr::from([192, 168, 1, 1]), 22)).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![3, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 1, 1, 0, 22]);

        // Test on the IPv6 address fd00::9:2 on port 300
        serializer = ByteString::default();
        assert_eq!(
            Neighbour::new((IpAddr::from([0xfd00, 0x0, 0x0, 0x0, 0x0, 0x0, 0x9, 0x2]), 300)).serialize(&mut serializer),
            Ok(())
        );
        assert_eq!(serializer.get(), vec![3, 18, 253, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 2, 1, 44]);
    }

    #[test]
    fn serialize_tlv_data() {
        let mut serializer = ByteString::default();
        assert_eq!(Data::new((999, 123, "test: H3llØ wörld!".to_owned())).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![
            4, 32, 0, 0, 0, 0, 0, 0, 3, 231, 0, 0, 0, 123, 116, 101, 115, 116, 58, 32, 72, 51, 108, 108, 195, 152, 32, 119, 195,
            182, 114, 108, 100, 33
        ]);
    }

    #[test]
    fn serialize_tlv_ack() {
        let mut serializer = ByteString::default();
        assert_eq!(Ack::new((999, 123)).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![5, 12, 0, 0, 0, 0, 0, 0, 3, 231, 0, 0, 0, 123]);
    }

    #[test]
    fn serialize_tlv_go_away() {
        let mut serializer = ByteString::default();
        assert_eq!(GoAway::new((3, "You are not following the protocole!".to_owned())).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![
            6, 37, 3, 89, 111, 117, 32, 97, 114, 101, 32, 110, 111, 116, 32, 102, 111, 108, 108, 111, 119, 105, 110, 103, 32, 116,
            104, 101, 32, 112, 114, 111, 116, 111, 99, 111, 108, 101, 33
        ]);
    }

    #[test]
    fn serialize_tlv_warning() {
        let mut serializer = ByteString::default();
        assert_eq!(Warning::new("This is not a warning…".to_owned()).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![
            7, 24, 84, 104, 105, 115, 32, 105, 115, 32, 110, 111, 116, 32, 97, 32, 119, 97, 114, 110, 105, 110, 103, 226, 128, 166
        ]);
    }

    #[test]
    fn serialize_tlv_whisper() {
        let mut serializer = ByteString::default();
        assert_eq!(Whisper::new((999, 1001, 123, "test: H3llØ wörld!".to_owned())).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![
            8, 40, 0, 0, 0, 0, 0, 0, 3, 231, 0, 0, 0, 0, 0, 0, 3, 233, 0, 0, 0, 123, 116, 101, 115, 116, 58, 32, 72, 51, 108, 108,
            195, 152, 32, 119, 195, 182, 114, 108, 100, 33
        ]);
    }

    #[test]
    fn serialize_tlv_other() {
        let mut serializer = ByteString::default();
        assert_eq!(Other::new((10, 9, vec![0, 1, 2, 3, 4, 5, 6, 7, 8])).serialize(&mut serializer), Ok(()));
        assert_eq!(serializer.get(), vec![10, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8]);
    }

    #[test]
    fn serialize_tlv_too_long() {
        let mut serializer = ByteString::default();
        assert!(Data::new((0, 0, "A".repeat(300))).serialize(&mut serializer).is_err());
    }
}

#[cfg(test)]
mod tlv_byte_deserialization {
    use std::net::IpAddr;

    use serde::Deserialize;

    use super::tlv::{Instantiable, Pad1};
    use super::Tlv;
    use crate::payload::serializer::ByteString;
    use crate::payload::tlv::ack::Ack;
    use crate::payload::tlv::data::Data;
    use crate::payload::tlv::go_away::GoAway;
    use crate::payload::tlv::hello::Hello;
    use crate::payload::tlv::neighbour::Neighbour;
    use crate::payload::tlv::other::Other;
    use crate::payload::tlv::padn::PadN;
    use crate::payload::tlv::warning::Warning;
    use crate::payload::tlv::Whisper;

    #[test]
    fn deserialize_tlv_pad1() {
        let mut serialized_tlv = ByteString::new(vec![0]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Pad1(Pad1::new(()))))
    }

    #[test]
    fn deserialize_tlv_padn() {
        let mut serialized_tlv = ByteString::new(vec![1, 3, 0, 0, 0]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::PadN(PadN::new(3))));

        serialized_tlv = ByteString::new(vec![1, 5, 0, 0, 0, 0, 0]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::PadN(PadN::new(5))));
    }

    #[test]
    fn deserialize_tlv_short_hello() {
        let mut serialized_tlv = ByteString::new(vec![2, 8, 12, 131, 23, 237, 191, 199, 95, 120]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Hello(Hello::new((901590660296433528, None)))));
    }

    #[test]
    fn deserialize_tlv_long_hello() {
        let mut serialized_tlv = ByteString::new(vec![2, 16, 12, 131, 23, 237, 191, 199, 95, 120, 0, 0, 0, 0, 0, 0, 0, 12]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Hello(Hello::new((901590660296433528, Some(12))))));
    }

    #[test]
    fn deserialize_tlv_neighbour() {
        // Test on the IPv4 address 192.168.1.1:22
        let mut serialized_tlv = ByteString::new(vec![3, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 1, 1, 0, 22]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Neighbour(Neighbour::new((IpAddr::from([192, 168, 1, 1]), 22)))));

        // Test on the IPv6 address fd00::9:2 on port 300
        let mut serialized_tlv = ByteString::new(vec![3, 18, 253, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 2, 1, 44]);
        assert_eq!(
            Tlv::deserialize(&mut serialized_tlv),
            Ok(Tlv::Neighbour(Neighbour::new((IpAddr::from([0xfd00, 0x0, 0x0, 0x0, 0x0, 0x0, 0x9, 0x2]), 300))))
        );
    }

    #[test]
    fn deserialize_tlv_data() {
        let mut serialized_tlv = ByteString::new(vec![
            4, 32, 0, 0, 0, 0, 0, 0, 3, 231, 0, 0, 0, 123, 116, 101, 115, 116, 58, 32, 72, 51, 108, 108, 195, 152, 32, 119, 195,
            182, 114, 108, 100, 33,
        ]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Data(Data::new((999, 123, "test: H3llØ wörld!".to_owned())))));
    }

    #[test]
    fn deserialize_tlv_ack() {
        let mut serialized_tlv = ByteString::new(vec![5, 12, 0, 0, 0, 0, 0, 0, 3, 231, 0, 0, 0, 123]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Ack(Ack::new((999, 123)))));
    }

    #[test]
    fn deserialize_tlv_go_away() {
        let mut serialized_tlv = ByteString::new(vec![
            6, 37, 3, 89, 111, 117, 32, 97, 114, 101, 32, 110, 111, 116, 32, 102, 111, 108, 108, 111, 119, 105, 110, 103, 32, 116,
            104, 101, 32, 112, 114, 111, 116, 111, 99, 111, 108, 101, 33,
        ]);
        assert_eq!(
            Tlv::deserialize(&mut serialized_tlv),
            Ok(Tlv::GoAway(GoAway::new((3, "You are not following the protocole!".to_owned()))))
        );
    }

    #[test]
    fn deserialize_tlv_warning() {
        let mut serialized_tlv = ByteString::new(vec![
            7, 24, 84, 104, 105, 115, 32, 105, 115, 32, 110, 111, 116, 32, 97, 32, 119, 97, 114, 110, 105, 110, 103, 226, 128, 166,
        ]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Warning(Warning::new("This is not a warning…".to_owned()))));
    }

    #[test]
    fn deserialize_tlv_whisper() {
        let mut serialized_tlv = ByteString::new(vec![
            8, 40, 0, 0, 0, 0, 0, 0, 3, 231, 0, 0, 0, 0, 0, 0, 3, 233, 0, 0, 0, 123, 116, 101, 115, 116, 58, 32, 72, 51, 108, 108,
            195, 152, 32, 119, 195, 182, 114, 108, 100, 33,
        ]);
        assert_eq!(
            Tlv::deserialize(&mut serialized_tlv),
            Ok(Tlv::Whisper(Whisper::new((999, 1001, 123, "test: H3llØ wörld!".to_owned()))))
        );
    }

    #[test]
    fn deserialize_tlv_other() {
        let mut serialized_tlv = ByteString::new(vec![10, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8]);
        assert_eq!(Tlv::deserialize(&mut serialized_tlv), Ok(Tlv::Other(Other::new((10, 9, vec![0, 1, 2, 3, 4, 5, 6, 7, 8])))));
    }
}
