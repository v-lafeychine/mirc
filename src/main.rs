#![feature(iter_advance_by)]

mod payload;
mod server;
mod ui;

use std::net::{Ipv6Addr, SocketAddrV6};
use std::sync::mpsc::{Receiver, Sender};
use std::time::{Duration, Instant};
use std::{io, thread};

use anyhow::Result;
use clap::Parser;
use crossterm::cursor::EnableBlinking;
use crossterm::event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyModifiers};
use crossterm::execute;
use crossterm::terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen};
use log::info;
use payload::Tlv;
use rand::Rng;
use server::neighbour::Neighbour;
use server::node::DataType;
use server::{Command, Data};
use tokio::net::UdpSocket;
use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use tokio::task::JoinHandle;
use tui::backend::CrosstermBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::{Frame, Terminal};
use ui::input::{Input, INPUT_MARGIN};
use ui::tabs::Tabs;

use crate::server::node::Node;

/// A MIRC client.
#[derive(Parser)]
#[command(version)]
struct Args {
    /// The address to listen on
    #[arg(default_value = "[::1]:8080")]
    ip: SocketAddrV6,

    /// The initial potential neighbours.
    potential: Vec<SocketAddrV6>,
}

async fn flatten<T>(handle: JoinHandle<Result<T>>) -> Result<T> {
    match handle.await {
        Ok(Ok(result)) => Ok(result),
        Ok(Err(err)) => Err(err),
        Err(err) => Err(err.into()),
    }
}

/// Main backend function : launch all tokio threads
#[tokio::main]
async fn mirc(
    ip: SocketAddrV6,
    node: Node,
    receiver_socket: tokio::sync::mpsc::Receiver<(SocketAddrV6, Tlv)>,
    sender_command: UnboundedSender<Command>,
    receiver_command: UnboundedReceiver<Command>,
    sender_data: UnboundedSender<Data>,
    system: Neighbour,
) -> Result<()> {
    env_logger::init();

    let socket = UdpSocket::bind(ip).await?;

    info!("Welcome to MIRC");

    tokio::try_join!(
        flatten(tokio::spawn(server::dispatcher(
            node,
            receiver_command,
            sender_command.clone(),
            sender_data.clone(),
            system.clone()
        ))),
        flatten(tokio::spawn(server::heartbeat_alive(sender_command.clone()))),
        flatten(tokio::spawn(server::heartbeat_sending(Duration::from_secs(10), sender_command.clone()))),
        flatten(tokio::spawn(server::flooding(sender_command.clone()))),
        flatten(tokio::spawn(server::world(socket, receiver_socket, sender_command, sender_data, system))),
    )
    .map(|_| ())
}

/// Reads user inputs and sends them
fn input_reader(sender: Sender<Event>) {
    let tick_rate = Duration::from_millis(10);

    let mut last_tick = Instant::now();

    loop {
        let timeout = tick_rate.checked_sub(last_tick.elapsed()).unwrap_or_else(|| Duration::from_secs(0));

        match event::poll(timeout) {
            Ok(true) => match event::read() {
                Ok(event) => sender.send(event).expect("Broken input pipe : cannot send the input received"),
                Err(err) => eprintln!("{err}"),
            },
            Ok(false) => {},
            Err(err) => println!("{err}"),
        }

        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now()
        }
    }
}

/// Update the frame of the TUI
///
/// Returns whether the loop should be broke
#[allow(clippy::too_many_arguments)]
fn update_frame<W: io::Write>(
    frame: &mut Frame<CrosstermBackend<W>>,
    receiver_input: &Receiver<Event>,
    receiver_data: &mut UnboundedReceiver<Data>,
    sender_data: &UnboundedSender<Data>,
    sender_command: &UnboundedSender<Command>,
    node_id: u64,
    tabs: &mut Tabs,
    input: &mut Input,
    system: Neighbour,
    everyone: Neighbour,
) -> bool {
    let size = frame.size();

    let vertical_chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(10), Constraint::Percentage(90)])
        .split(size);

    let horizontal_chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(INPUT_MARGIN.vertical)
        .constraints([Constraint::Percentage(90), Constraint::Percentage(10)])
        .split(vertical_chunks[1]);

    let close_tui = match match receiver_input.try_recv() {
        Ok(event) => event,
        Err(_) => Event::FocusGained,
    } {
        Event::FocusGained => false,
        Event::FocusLost => false,
        Event::Key(k) => {
            match k.code {
                event::KeyCode::Backspace => {
                    input.input_backspace();
                    input.cursor_left()
                },
                event::KeyCode::Enter => {
                    let chat = tabs.current_chat();

                    if chat.neighbour == system {
                        sender_data
                            .send(Data {
                                neighbour: system,
                                content: DataType::System("Cannot send messages in \"System\" channel!".to_owned()),
                            })
                            .expect("Broken data pipe: cannot send data");
                    } else if chat.neighbour == everyone {
                        let message = input.pop_input();
                        let nonce = rand::random::<u32>();

                        chat.add_message(node_id, nonce, input.nick.clone(), message.clone(), false);
                        input.reset_cursor();

                        sender_command
                            .send(Command::CreateFlood(nonce, message))
                            .expect("Broken command pipe: cannot send command");
                    } else {
                        let message = input.pop_input();
                        let nonce = rand::random::<u32>();

                        chat.add_message(node_id, nonce, input.nick.clone(), message.clone(), false);
                        input.reset_cursor();

                        sender_command
                            .send(Command::Whisper(chat.neighbour.clone(), nonce, message))
                            .expect("Broken command pipe: cannot send command");
                    }
                },
                event::KeyCode::Left => input.cursor_left(),
                event::KeyCode::Right => input.cursor_right(),
                event::KeyCode::Up => {
                    if k.modifiers == KeyModifiers::ALT {
                        tabs.tab_up()
                    }
                },
                event::KeyCode::Down => {
                    if k.modifiers == KeyModifiers::ALT {
                        tabs.tab_down()
                    }
                },
                event::KeyCode::Char(c) => {
                    input.input_insert(c.to_string());
                    input.cursor_right()
                },
                event::KeyCode::Esc => {},
                event::KeyCode::CapsLock => {},
                event::KeyCode::PageUp => input.reset_cursor(),
                _ => {},
            };

            k.code == crossterm::event::KeyCode::Char('c') && k.modifiers == KeyModifiers::CONTROL
        },
        Event::Mouse(_) => false,
        Event::Paste(_) => false,
        Event::Resize(_, _) => false,
    };

    if let Ok(data) = receiver_data.try_recv() {
        match data.content {
            DataType::Message(id, nonce, text) => {
                let previous_neighbour = tabs.current_chat().neighbour.clone();

                let mut splited_text = text.split(':');
                let nick = splited_text.next().unwrap().to_owned();
                let message = splited_text.collect::<Vec<&str>>().join(":");

                // Add new message to the "everyone" tab
                tabs.set_tab(&everyone);
                let chat = tabs.current_chat();
                chat.add_message(id, nonce, nick, message, true);
                tabs.set_tab(&previous_neighbour);
            },
            DataType::Whisper(id, nonce, text) => {
                let previous_neighbour = tabs.current_chat().neighbour.clone();

                let mut splited_text = text.split(':');
                let nick = splited_text.next().unwrap().to_owned();
                let message = splited_text.collect::<Vec<&str>>().join(":");

                // Add new message to the correct tab
                tabs.set_tab(&data.neighbour);
                let chat = tabs.current_chat();
                chat.add_message(id, nonce, nick.to_owned(), message, true);
                tabs.set_tab(&previous_neighbour);

                // Update nick of tab which has received a new message
                let Some(_) = tabs.set_nick(&data.neighbour, nick) else {
                    unreachable!("The previous neighbour is a copy of an existing one")
                };
            },
            DataType::System(text) => {
                tabs.get_tab(&data.neighbour).expect("Cannot find system chat").add_message(0, 0, "System".to_owned(), text, true)
            },
            DataType::NewNeighbour => {
                let neighbour = data.neighbour;
                if !tabs.contains(&neighbour) {
                    tabs.insert(neighbour);
                }
            },
            DataType::ReceivedMessage(id, nonce) => tabs.received_message(id, nonce),
        }
    }

    let cursor = input.cursor;
    let mut chat = tabs.current_chat().clone();
    frame.render_widget(tabs, vertical_chunks[0]);
    frame.render_widget(&mut chat, horizontal_chunks[0]);
    frame.render_widget(input, horizontal_chunks[1]);
    frame.set_cursor(
        horizontal_chunks[1].inner(&INPUT_MARGIN).x + cursor % horizontal_chunks[1].inner(&INPUT_MARGIN).width,
        horizontal_chunks[1].inner(&INPUT_MARGIN).y + (cursor / horizontal_chunks[1].inner(&INPUT_MARGIN).width).min(1),
    );

    if close_tui {
        sender_command.send(Command::Leave).expect("Broken command pipe: cannot send command");
    }

    close_tui
}

fn main() -> Result<(), io::Error> {
    let args = Args::parse();

    enable_raw_mode()?;

    // TUI creation
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    let mut tabs = Tabs::default();
    let system = Neighbour::new(0, SocketAddrV6::new(Ipv6Addr::from(0), 0, 0, 0), "System".to_owned());
    let everyone = Neighbour::new(1, SocketAddrV6::new(Ipv6Addr::from(0), 0, 0, 0), "Everyone".to_owned());
    tabs.insert(system.clone());
    tabs.insert(everyone.clone());

    let nick: String = rand::thread_rng().sample_iter(&rand::distributions::Alphanumeric).take(8).map(char::from).collect();
    let mut input = Input {
        nick: nick.clone(),
        input: "".to_owned(),
        cursor: 0,
    };

    // Sender and receiver
    let (sender_command, receiver_command) = mpsc::unbounded_channel();
    let (sender_data, mut receiver_data) = mpsc::unbounded_channel();
    let (sender_socket, receiver_socket) = mpsc::channel(1024);

    // Self node
    let ip = args.ip;
    let node = Node::new(sender_socket, args.ip, nick, args.potential.into_iter().collect());
    let node_id = node.id;

    let sender_data_clone = sender_data.clone();
    let sender_command_clone = sender_command.clone();
    let system_clone = system.clone();
    thread::spawn(move || mirc(ip, node, receiver_socket, sender_command_clone, receiver_command, sender_data_clone, system_clone));

    // Input sender and receiver : transmit user events (key pressed, mouse, etc)
    let (sender_input, receiver_input) = std::sync::mpsc::channel();

    // To force refresh once
    let _ = sender_input.send(Event::Key(event::KeyEvent::new(event::KeyCode::PageUp, event::KeyModifiers::empty())));

    // Update TUI
    thread::spawn(|| input_reader(sender_input));
    execute!(terminal.backend_mut(), EnableBlinking)?;
    let mut force_quit = false;
    while !force_quit {
        terminal.draw(|f| {
            force_quit = update_frame(
                f,
                &receiver_input,
                &mut receiver_data,
                &sender_data,
                &sender_command,
                node_id,
                &mut tabs,
                &mut input,
                system.clone(),
                everyone.clone(),
            )
        })?;
    }

    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen, DisableMouseCapture)?;

    Ok(())
}
